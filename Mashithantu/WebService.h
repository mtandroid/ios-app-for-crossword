//
//  WebService.h
//  Mashithantu
//
//  Created by Shyam on 25/01/16.
//  Copyright © 2016 Anupa KA. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^MashithantuCompletion)(NSDictionary* resultDictionary, NSError* error);

@interface WebService : NSObject

+ (id)api;

#pragma mark - GET METHODS

- (void)getMethodWithOptions:(NSString *)getURLString completion:(MashithantuCompletion)completion;

#pragma mark - POST METHODS

- (void)postMethodWithOptions:(NSString *)parameters urlString:(NSString *)postURLString completion:(MashithantuCompletion)completion;
- (void)postMethodWithImage:(NSString *)postURLString profileData:(NSData *)profileData completion:(MashithantuCompletion)completion;

@end

