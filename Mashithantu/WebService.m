//
//  WebService.m
//Mashithantu
//
//  Created by Shyam on 25/01/16.
//  Copyright © 2016 Anupa KA. All rights reserved.
//

#import "WebService.h"
#import "Constants.h"

@implementation WebService

+ (id)api {
    return  [[self alloc] init];
}

#pragma mark - GET METHOD

- (void)getMethodWithOptions:(NSString*)getURLString completion:(MashithantuCompletion)completion {
    
    if (!completion) return;
    
    NSLog(@"URL : %@", [NSString stringWithFormat:@"%@%@",API_BASE_URL,getURLString] );
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",API_BASE_URL,getURLString]]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error) {
            completion(nil, error);
        } else {
            NSError *err = nil;
            NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            //NSLog(@"dataDict : %@",dataDict);
            if (!err) {
                completion(dataDict, nil);
            } else {
                completion(nil, err);
            }
        }
    }];
}


#pragma mark - POST METHOD

- (void)postMethodWithOptions:(NSString *)parameters urlString:(NSString *)postURLString completion:(MashithantuCompletion)completion {
    
    if (!completion) return;
    
    NSLog(@"postURLString : %@",postURLString);
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",API_BASE_URL,postURLString]];
    
    NSString *JSONString = [NSString stringWithFormat:@"%@",parameters];
    NSLog(@"JSONString : %@",JSONString);
    NSData *JSONBody = [JSONString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *loginRequest = [[NSMutableURLRequest alloc] initWithURL:url];
    loginRequest.HTTPMethod = @"POST";
    loginRequest.HTTPBody = JSONBody;
    
    NSOperationQueue *queue = [NSOperationQueue new];
    
    [NSURLConnection sendAsynchronousRequest:loginRequest
                                       queue:queue
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error) {
             completion(nil, error);
         } else {
             NSError *err = nil;
             NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
             //NSLog(@"dataDict : %@",dataDict);
             if (!err) {
                 completion(dataDict, nil);
             } else {
                 completion(nil, err);
             }
         }
     }];
}

- (void)postMethodWithImage:(NSString *)postURLString profileData:(NSData *)profileData completion:(MashithantuCompletion)completion {
    
    if (!completion) return;
    
    NSLog(@"URL : %@",[NSString stringWithFormat:@"%@%@",API_BASE_URL,postURLString]);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",API_BASE_URL,postURLString]] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:1200.0];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [request setHTTPMethod:@"POST"];
    NSString *boundary = @"---------------------------14737809831466499882746641449";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"profile_image\"; filename=\"iPhone_1.jpg\"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:profileData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    // now lets make the connection to the web
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *returnData, NSError *error)
     {
         if ([returnData length] >0 && error == nil)
         {
             // DO YOUR WORK HERE
             if (error) {
                 completion(nil, error);
             } else {
                 NSError *err = nil;
                 NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:returnData options:NSJSONReadingMutableContainers error:&err];
                 if (!err) {
                     completion(dataDict, nil);
                 } else {
                     completion(nil, err);
                 }
             }
         }
         else if ([returnData length] == 0 && error == nil)
         {
             NSLog(@"Nothing was downloaded.");
         }
         else if (error != nil){
             NSLog(@"Error = %@", error);
         }
     }];
    
}

@end
