//
//  RulesViewController.m
//  Mashithantu
//
//  Created by Nidhin Baby on 24/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "RulesViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "CustomCell.h"
#import "Menu.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"

@interface RulesViewController ()
{
    Menu *objMenu;
}
@end

@implementation RulesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
        
    objMenu = [[Menu alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        
        [objMenu .tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu .helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu .exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    rulesMutableArray = [[NSMutableArray alloc] init];
    prizesMutableArray = [[NSMutableArray alloc] init];
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth = 5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(rulesApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void) rulesApi {

    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=rulesprizes", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"%@", resultDictionary);
        
       NSArray *prizesArray = [resultDictionary objectForKey:@"prizes"];
        
        NSArray *rulesArray = [ resultDictionary objectForKey:@"rules"];
        
        if (prizesArray.count == 0) {
            
            NSLog(@"Empty");
        }
        else {
            
            [prizesMutableArray addObjectsFromArray:prizesArray];
        }
        
        if (rulesArray.count == 0) {
            
            NSLog(@"RULE EMPTY");
        }
        else {
            
            [rulesMutableArray addObjectsFromArray:rulesArray];
        }
        
        [rulesTable reloadData];
        
        consolationArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < prizesMutableArray.count; i++) {
            
            NSDictionary * prizesDictionary = [prizesMutableArray objectAtIndex:i];
            
            if ([[prizesDictionary valueForKeyPath:@"winner_position"] isEqualToString:@"1"]) {
                
                firstprizeLabel.text = [prizesDictionary valueForKeyPath:@"prize_description"];
            }
            else if ([[prizesDictionary valueForKeyPath:@"winner_position"] isEqualToString:@"2"]) {
                
                secondprizeLabel.text = [prizesDictionary valueForKeyPath:@"prize_description"];
            }
            else if ([[prizesDictionary valueForKeyPath:@"winner_position"] isEqualToString:@"3"]) {
                
                thirdprizeLabel.text = [prizesDictionary valueForKeyPath:@"prize_description"];
            }
            else {
                
                [consolationArray addObject:[prizesMutableArray objectAtIndex:i]];
            }
        }
        
        [prizesTable reloadData];
        
        [spinner stopAnimating];
        
        footerView.frame = CGRectMake(footerView.frame.origin.x, footerView.frame.origin.y, footerView.frame.size.width, 210 + prizesTable.contentSize.height);
        rulesTable.tableFooterView = footerView;
    }];
}

-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton {
    
    objMenu.hidden = TRUE;
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Profile";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

-(void)exitButton {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if([[resultDictionary valueForKeyPath:@"errorcode"]intValue] == 200) {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == prizesTable) {
        
        return consolationArray.count;
    }
    else {
        
        return [rulesMutableArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    if (tableView == prizesTable) {
        
        NSDictionary * prizesDictionary = [consolationArray objectAtIndex:indexPath.row];
        
            cell.rulesLabel.text = [NSString stringWithFormat:@"%@",[prizesDictionary valueForKeyPath:@"prize_description"]];
        
            cell.rulesImageView.layer.borderWidth = 1.0;
            cell.rulesImageView.layer.borderColor = [[UIColor colorWithRed:59.0/255.0 green:104.0/255.0 blue:105.0/255.0 alpha:1]CGColor];
            cell.rulesImageView.layer.cornerRadius = cell.rulesImageView.frame.size.width/2;
            cell.rulesImageView.clipsToBounds = YES;
        
            if (indexPath.row == consolationArray.count - 1) {
                
                cell.rulesDashImageView.hidden = TRUE;
            }
            else {
                
                cell.rulesDashImageView.hidden = FALSE;
            }
        }
    else {
        
        NSDictionary * rulesDictionary = [rulesMutableArray objectAtIndex:indexPath.row];
        
        cell.rulenumberLabel.text = [NSString stringWithFormat:@"%@",[rulesDictionary valueForKeyPath:@"id"]];
        
        cell.rulesLabel.text = [NSString stringWithFormat:@"%@",[rulesDictionary valueForKeyPath:@"rule_description"]];
        cell.rulesLabel.numberOfLines = 0;
        [cell.rulesLabel sizeToFit];
        
        cell.rulesImageView.layer.borderWidth = 1.0;
        cell.rulesImageView.layer.borderColor = [[UIColor colorWithRed:59.0/255.0 green:104.0/255.0 blue:105.0/255.0 alpha:1]CGColor];
        cell.rulesImageView.layer.cornerRadius = cell.rulesImageView.frame.size.width/2;
        cell.rulesImageView.clipsToBounds = YES;
        
        if (indexPath.row == rulesMutableArray.count - 1) {
            
            cell.rulesDashImageView.hidden = TRUE;
        }
        else {
            
            cell.rulesDashImageView.hidden = FALSE;
        }
    }
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
