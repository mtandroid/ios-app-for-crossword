//
//  VerificationViewController.m
//  Mashithantu
//
//  Created by Shyam Alanghat on 14/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "VerificationViewController.h"
#import "RegisterViewController.h"
#import "UIView+Toast.h"
#import "Constants.h"
#import "HomeViewController.h"

@interface VerificationViewController ()

@end

@implementation VerificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden= YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    nextButton.layer.borderWidth = 1.5;
    nextButton.layer.borderColor = [[UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0] CGColor];
    nextButton.layer.cornerRadius = 3.0;
    
    verificationScroll.contentSize = CGSizeMake(320, 568);
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:registerLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 22)];
    [registerLabel setAttributedText:clueString];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard
{
    [verificationText resignFirstResponder];
}


-(IBAction)nextButton:(id)sender{
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        if (verificationText.text.length == 0) {
            
            [self.view makeToast:@"Please enter the verification code" duration:3.0 position:CSToastPositionCenter];
            [verificationText becomeFirstResponder];
        }
        else {

            nextButton.enabled = NO;
            
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth =5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            
            [self performSelector:@selector(verificationApi) withObject:spinner afterDelay:0.0];
        }
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [verificationText resignFirstResponder];
    return YES;
}

-(void)verificationApi {
    
     [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=registerverify&deviceid=%@&userid=%@&verifycode=%@&id=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [self.useridString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[verificationText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], self.idString, [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {

         NSLog(@"VerificationResponse%@",resultDictionary);
         
          if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200) {
             
              HomeViewController *objhome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
              [self.navigationController pushViewController:objhome animated:YES];
         }
         else {
             
             [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
         }
         [spinner stopAnimating];
         nextButton.enabled = YES;
     }];
    
}


-(IBAction)createaccountButton:(id)sender {
    
    RegisterViewController *obRegister = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:obRegister animated:YES];
}

-(IBAction)backButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
