//
//  CustomCell.h
//  Mashithantu
//
//  Created by Shyam on 19/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface CustomCell : UITableViewCell {
    
}

@property(nonatomic,weak) IBOutlet UILabel* eventLabel,*rankLabel,*cwcLabel,*playtypeLabel,*scoreLabel,*BonusLabel,*ranknowLabel,*calenderLabel,*toppereventLabel,*serialnoLabel,*toppernameLabel,*topperidLabel,*topperscoreLabel,*sheduletopicLabel,*statusLabel,*playernameLabel,*dateLabel,*calenderdateLabel,*ratedLabel,*suggestionnameLabel,*rulenumberLabel,*rulesLabel;
@property(nonatomic,weak) IBOutlet UIButton* playButton,*cellratebutton,*calendarPlayButton,*selectedPlayButton;
@property(nonatomic,weak) IBOutlet UIImageView* gamestatusImageView,*serialCircleImageView,*bonusImageView,*calenderImageView,*rulesImageView,*rulesDashImageView,*rateImageView,*giftImageView,*playImageView,*calendarImageView;
@property(nonatomic,weak) IBOutlet HCSStarRatingView * rateview;


@end
