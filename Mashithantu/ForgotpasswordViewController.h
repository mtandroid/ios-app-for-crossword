//
//  ForgotpasswordViewController.h
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface ForgotpasswordViewController : UIViewController{
    
    IBOutlet UIScrollView *forgotScroll;
    IBOutlet UITextField *emailText;
    IBOutlet UIButton *sendOTPButton,*cancelButton;
    WebService *webService;
    Reachability *reach;
    MMMaterialDesignSpinner *spinner;
}

-(IBAction)sendOTPButton:(id)sender;
-(IBAction)cancelButton:(id)sender;

@end
