//
//  HomeViewController.h
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLAnimatedImage.h"
#import "Reachability.h"
#import "WebService.h"
#import "Menu.h"

@interface HomeViewController : UIViewController {
    
    IBOutlet UIButton *userButton,*sheduleButton,*prizesButton,*questionsButton,*playButton,*menuButton;
    IBOutlet UILabel *nameLabel,*playLabel;
    IBOutlet UIView *playView;
    IBOutlet FLAnimatedImageView *loadImage;
    NSTimer *playTimer;
    NSString *playTimeStr;
    WebService *webService;
    Reachability *reach;
    Menu *objMenu;
}

-(IBAction)userButton:(id)sender;
-(IBAction)sheduleButton:(id)sender;
-(IBAction)prizesButton:(id)sender;
-(IBAction)questionsButton:(id)sender;
-(IBAction)playButton:(id)sender;
-(IBAction)menuButton:(id)sender;


@end
