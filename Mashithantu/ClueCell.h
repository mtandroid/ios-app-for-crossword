//
//  ClueCell.h
//  Mashithantu
//
//  Created by 2Base MacBook Pro on 30/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClueCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *clueLabel;

@end
