//
//  PlayViewController.h
//  Mashithantu
//
//  Created by 2Base MacBook Pro on 29/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "WebService.h"
#import "CluesViewController.h"

@interface PlayViewController : UIViewController <ClueDelegate>
{
    WebService *webService;
    Reachability *reach;
    UIActivityIndicatorView *activityIndicator;
    NSMutableArray *gridArray,*deadcellsArray,*questionsArray,*selectedArray,*answersArray,*fullanswerArray,*relatedIndicesArray,*multipleArray,*gridtapArray;
    int rowValue, columnValue;
    IBOutlet UITableView *clueTable;
    IBOutlet UIButton *homeButton,*scoreButton,*menuButton,*clearButton,*clueButton,*keypadButton, *tutorialButton,*hidetutorialButton;
    IBOutlet UITextField *keypadText;
    IBOutlet UILabel *fillLabel;
    IBOutlet UIView *tutorialView;
    NSInteger clickedSection,clickedRow;
    NSString *rotationString,*cluelengthString,*typedString;
}

@property (nonatomic, retain) NSString *crosswordID;

-(IBAction)homeButton:(id)sender;
-(IBAction)scoreButton:(id)sender;
-(IBAction)menuButton:(id)sender;
-(IBAction)clearButton:(id)sender;
-(IBAction)clueButton:(id)sender;
-(IBAction)keypadButton:(id)sender;
-(IBAction)tutorialButton:(id)sender;
-(IBAction)hidetutorialButton:(id)sender;

@end
