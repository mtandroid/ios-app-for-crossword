//
//  HomeViewController.m
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "ToppersGeneralViewController.h"
#import "SheduleViewController.h"
#import "RulesViewController.h"
#import "Constants.h"
#import "ProfileViewController.h"
#import "PlayViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    objMenu = [[Menu alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    playView.hidden = TRUE;

    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        nameLabel.text = @"Welcome Guest";
        playLabel.text = @"Play Latest Crossword as guest";
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        [objMenu .tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        nameLabel.text =  [NSString stringWithFormat:@"Welcome %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"]];
        playLabel.text = @"tap to play!";
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void)viewWillAppear:(BOOL)animated {
    
    playTimeStr = @"00";
}

-(IBAction)sheduleButton:(id)sender {
    
    SheduleViewController * objShedule = [self.storyboard instantiateViewControllerWithIdentifier:@"SheduleViewController"];
    [self.navigationController pushViewController:objShedule animated:YES];
}

-(IBAction)prizesButton:(id)sender {
    
    ToppersGeneralViewController *objtopper = [self.storyboard instantiateViewControllerWithIdentifier:@"ToppersGeneralViewController"];
    [self.navigationController pushViewController:objtopper animated:YES];
}

-(IBAction)userButton:(id)sender {
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
    else {
        
        ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
        objProfile.screenString = @"Profile";
        [self.navigationController pushViewController:objProfile animated:YES];
    }
}

-(IBAction)playButton:(id)sender {
    
    loadImage.animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Test" ofType:@"gif"]]];
    
    playView.hidden = FALSE;
    
    playTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(playTimerRunning) userInfo:nil repeats:YES];
}

-(void)playTimerRunning
{
    NSInteger second;
    second = [[playTimeStr substringWithRange:NSMakeRange(0, 2)] integerValue];
    
    second ++;
    if (second >= 60)
    {
        second = 0;
    }
    
    playTimeStr = [NSString stringWithFormat:@"%@", second > 9 ? [@(second) stringValue] : [NSString stringWithFormat:@"0%@",[@(second) stringValue]]];
    
    if ([playTimeStr intValue] > 5) {
        
        [playTimer invalidate];
        playTimer = nil;
        
        playView.hidden = TRUE;
        
        PlayViewController *objPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
        objPlay.crosswordID = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"CrosswordID"]];
        [self.navigationController pushViewController:objPlay animated:YES];
    }
}

-(IBAction)questionsButton:(id)sender {
    
    RulesViewController *objrules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objrules animated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton {
    
    objMenu.hidden = TRUE;
    RulesViewController *objrules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objrules animated:NO];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;

    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Profile";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

-(void)exitButton {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if([[resultDictionary valueForKeyPath:@"errorcode"]intValue] == 200){
                
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults]synchronize];

                [self.navigationController popToRootViewControllerAnimated:NO];
                
            }
        }];
}
    else {
        [alertView dismissWithClickedButtonIndex:1 animated:NO];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
