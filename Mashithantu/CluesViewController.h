//
//  CluesViewController.h
//  Mashithantu
//
//  Created by 2Base MacBook Pro on 28/12/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ClueDelegate;

@interface CluesViewController : UIViewController
{
    IBOutlet UILabel *downLabel;
    IBOutlet UITableView *allclueTable;
}

@property (nonatomic, unsafe_unretained) id <ClueDelegate> clueDelegate;

@property (nonatomic, strong) NSMutableArray *questionsArray;

@end

@protocol ClueDelegate <NSObject>

-(void)updateClue:(NSDictionary *)clueDict;

@end