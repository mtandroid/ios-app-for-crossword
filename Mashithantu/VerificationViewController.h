//
//  VerificationViewController.h
//  Mashithantu
//
//  Created by Shyam Alanghat on 14/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface VerificationViewController : UIViewController{
    
    IBOutlet UIScrollView *verificationScroll;
    IBOutlet UITextField *verificationText;
    IBOutlet UIButton *createaccountButton,*backButton,*nextButton;
    IBOutlet UILabel *registerLabel;
    WebService *webService;
    Reachability *reach;
    MMMaterialDesignSpinner *spinner;
}

@property(nonatomic,retain) NSString *useridString,*idString;

-(IBAction)createaccountButton:(id)sender;
-(IBAction)backButton:(id)sender;
-(IBAction)nextButton:(id)sender;

@end
