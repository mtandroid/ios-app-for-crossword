//
//  RegisterViewController.h
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface RegisterViewController : UIViewController {
    
    IBOutlet UITextField *firstnameText,*lastnameText,*useridText,*emailidText,*passwordText,*captchaTextfield;
    IBOutlet UIScrollView *registerScroll;
    IBOutlet UIView * captchaView;
    IBOutlet UIButton *createButton,*loginhereButton,*showhideButton,*reloadButton,*verifyButton;
    NSArray *arrCapElements;
    NSString * useridString;
    IBOutlet UILabel *captchaLabel,*loginLabel;
    IBOutlet UIImageView *firstnameImageView,*lastnameImageView,*useridImageView,*emailidImageView,*passwordImageView;
    WebService * webService;
    Reachability * reach;
    MMMaterialDesignSpinner *spinner;
    UIAlertView * useridAlert;
    UIActivityIndicatorView * activityIndicator;
}

-(IBAction)createButton:(id)sender;
-(IBAction)loginhereButton:(id)sender;
-(IBAction)showhideButton:(id)sender;
- (IBAction)Reload_Action:(id)sender;
- (IBAction)verifyButton:(id)sender;

@end
