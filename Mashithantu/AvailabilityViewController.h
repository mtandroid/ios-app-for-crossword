//
//  AvailabilityViewController.h
//  Mashithantu
//
//  Created by Shyam Alanghat on 15/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface AvailabilityViewController : UIViewController{
    
    IBOutlet UIScrollView *availabilityScroll;
    IBOutlet UITableView *availabilityTable;
    IBOutlet UITextField *useridText;
    IBOutlet UIButton *confirmButton;
    NSMutableArray *suggestionArray;
    NSString *useridString;
    WebService *webService;
    Reachability *reach;
    MMMaterialDesignSpinner *spinner;
}

@property(nonatomic,retain) NSArray *availabilityArray;
@property (nonatomic, retain) NSString *nameString,*emailString;

-(IBAction)confirmButton:(id)sender;


@end
