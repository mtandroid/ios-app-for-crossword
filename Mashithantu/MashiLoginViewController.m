//
//  MashiLoginViewController.m
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "MashiLoginViewController.h"
#import "RegisterViewController.h"
#import "ForgotpasswordViewController.h"
#import "HomeViewController.h"
#import "VerificationViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"

@interface MashiLoginViewController ()

@end

@implementation MashiLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    webService = [WebService api];
    
    loginScroll.contentSize = CGSizeMake(320, 568);
    
    emailText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"E-Mail ID/Username" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    passwordText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:registerLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 22)];
    [registerLabel setAttributedText:clueString];
    
    emailText.text = [[NSUserDefaults standardUserDefaults]valueForKey:@"UserName"];
    
    loginButton.layer.borderWidth = 1.5;
    loginButton.layer.cornerRadius = 3.0;
    loginButton.layer.borderColor = [[UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0] CGColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard
{
    [emailText resignFirstResponder];
    [passwordText resignFirstResponder];
}


  -(IBAction)loginButton:(id)sender {

      if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
          if (emailText.text.length == 0) {
            
              [self.view makeToast:@"Please enter your user id" duration:3.0 position:CSToastPositionCenter];
              [emailText becomeFirstResponder];
          }
          else if (passwordText.text.length == 0) {
            
              [self.view makeToast:@"Please enter your password" duration:3.0 position:CSToastPositionCenter];
              [passwordText becomeFirstResponder];
          }
          else {
            
              loginButton.enabled = NO;
            
              [spinner stopAnimating];
              //Activityindicator for loading the history
              spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
              spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
              spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
              spinner.lineWidth =5.0;
              spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
              [self.view addSubview:spinner];
              [spinner startAnimating];
            
              [self performSelector:@selector(loginApi) withObject:spinner afterDelay:0.0];
        }
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)loginApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=login&deviceid=%@&userid=%@&pass=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [emailText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [passwordText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Login Response %@",resultDictionary);
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"AppInside" forKey:@"AppStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:[resultDictionary objectForKey:@"userid"] forKey:@"UserId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setValue:emailText.text forKey:@"UserName"];
            [[NSUserDefaults standardUserDefaults] synchronize];

             HomeViewController * objhome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:objhome animated:YES];
        }
        else if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 231) {
            
            VerificationViewController * objVerify = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
            objVerify.useridString = [NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"userid"]];
            objVerify.idString = [NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"id"]];
            [self.navigationController pushViewController:objVerify animated:YES];
        }
        else {
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
            }
        }
        [spinner stopAnimating];
        loginButton.enabled = YES;
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (textField == emailText) {
        
        usernameImageView.backgroundColor = [UIColor colorWithRed:181.0/255.0 green:55.0/255.0 blue:94.0/255.0 alpha:1.0];
        passwordImageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }
    else if (textField==passwordText) {
        
        passwordImageView.backgroundColor = [UIColor colorWithRed:181.0/255.0 green:55.0/255.0 blue:94.0/255.0 alpha:1.0];
        usernameImageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }
    else {
        usernameImageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
        passwordImageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == emailText) {
        
        [emailText resignFirstResponder];
        [passwordText becomeFirstResponder];
    }
    else if (textField==passwordText) {
        [passwordText resignFirstResponder];
    }
    return YES;
}

-(IBAction)backButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)registerButton:(id)sender {
    
    RegisterViewController * objRegister = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:objRegister animated:YES];
}

-(IBAction)forgotButton:(id)sender {
    
    ForgotpasswordViewController * objFor = [self.storyboard instantiateViewControllerWithIdentifier:@"Forgot"];
    [self.navigationController pushViewController:objFor animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
