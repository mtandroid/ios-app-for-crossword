//
//  Menu.m
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "Menu.h"

@implementation Menu
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        NSLog(@"initWithFrame");
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"Menu" owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
        mainView.frame = self.bounds;
    }
    return self;
}
    
@end
