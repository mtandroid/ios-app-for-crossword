//
//  MashiLoginViewController.h
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface MashiLoginViewController : UIViewController {
    
    IBOutlet UIScrollView *loginScroll;
    IBOutlet UITextField *emailText,*passwordText;
    IBOutlet UIButton *forgotButton,*loginButton,*registerButton,*backButton;
    IBOutlet UIImageView *usernameImageView,*passwordImageView;
    IBOutlet UILabel *registerLabel;
    WebService *webService;
    Reachability *reach;
    MMMaterialDesignSpinner *spinner;
}

-(IBAction)forgotButton:(id)sender;
-(IBAction)loginButton:(id)sender;
-(IBAction)registerButton:(id)sender;
-(IBAction)backButton:(id)sender;

@end
