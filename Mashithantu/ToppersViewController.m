//
//  ToppersViewController.m
//  Mashithantu
//
//  Created by Shyam on 25/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ToppersViewController.h"
#import "CustomCell.h"
#import "Reachability.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "SVPullToRefresh.h"
#import "ProfileViewController.h"
#import "RulesViewController.h"
#import "MMMaterialDesignSpinner.h"
#import "Menu.h"

@interface ToppersViewController () {
    
    Menu *objMenu;
    MMMaterialDesignSpinner *spinner;
}

@end

@implementation ToppersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;

    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    objMenu = [[Menu alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    
    objMenu.hidden = TRUE;
    
    objMenu.guestView.hidden = TRUE;
    objMenu.userView.hidden = FALSE;
    
    [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
    [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
    [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
    [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
    [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
        
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        if ([self.conditionString isEqualToString:@"Event"]) {
        
            topicLabel.hidden = TRUE;
            
            eventnameLabel.frame = CGRectMake(eventnameLabel.frame.origin.x, 90, eventnameLabel.frame.size.width, 50);
            
            [self performSelector:@selector(topperswithpageApi) withObject:nil afterDelay:0.0];
        }
        else if ([self.conditionString isEqualToString:@"Cross"]) {
            
            [self performSelector:@selector(topperscwdwithpageApi) withObject:nil afterDelay:0.0];
        }
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)topperswithpageApi {

    topperpage = 0;

    topperArray = [[NSMutableArray alloc] init];
    
    [spinner stopAnimating];
    spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(toppersApi) withObject:spinner afterDelay:0.0];
    
    __weak ToppersViewController *weakSelf = self;
    
    [toppersTable addInfiniteScrollingWithActionHandler:^{
        
        [weakSelf toppersApi];
    }];
}

-(void)toppersApi {
    
    eventnameLabel.text = self.eventString;
    
    eventnameLabel.textColor = [UIColor whiteColor];
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=topper&event=%@&page=%d&pagesize=10", [self.eventString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], topperpage] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Topperdictionary : %@",resultDictionary);
        
        NSArray *resultArray = [resultDictionary objectForKey:@"toppers"];
        
        if (resultArray.count == 0) {
            
            [self.view makeToast:@"End Of the List" duration:3.0 position:CSToastPositionTop];
        }
        else {
            
            topperpage = topperpage + 1;
            [topperArray addObjectsFromArray:resultArray];
        }
        [toppersTable reloadData];
        [toppersTable.infiniteScrollingView stopAnimating];
        [spinner stopAnimating];
    }];
    
}

-(void)topperscwdwithpageApi {
    
    topperpage = 0;
    
    topperArray = [[NSMutableArray alloc] init];
    
    [spinner stopAnimating];
    spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(topperscwdApi) withObject:spinner afterDelay:0.0];
    
    __weak ToppersViewController *weakSelf = self;
    
    [toppersTable addInfiniteScrollingWithActionHandler:^{
        
        [weakSelf topperscwdApi];
    }];
}

-(void) topperscwdApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=topper&cw_id=%@&page=%d&pagesize=16", [self.eventString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],topperpage] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Topperdictionary : %@",resultDictionary);
        
        topicLabel.text = [NSString stringWithFormat:@"%@", [resultDictionary valueForKeyPath:@"topic"]];
        
        eventnameLabel.text = [NSString stringWithFormat:@"(%@ by %@)", self.eventString, [resultDictionary valueForKeyPath:@"created"]];
        
        eventnameLabel.textColor = [UIColor colorWithRed:71.0/255.0 green:96.0/255.0 blue:116.0/255.0 alpha:1.0];
        
        NSArray *resultArray = [resultDictionary objectForKey:@"toppers"];
        
        if (resultArray.count == 0) {
            
            [self.view makeToast:@"End Of the List" duration:3.0 position:CSToastPositionTop];
        }
        else {
            
            topperpage = topperpage + 1;
            [topperArray addObjectsFromArray:resultArray];
        }
        [toppersTable reloadData];
        [toppersTable.infiniteScrollingView stopAnimating];
        [spinner stopAnimating];
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [topperArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    NSDictionary * topperDictionary = [topperArray objectAtIndex:indexPath.row];
    
    cell.serialCircleImageView.layer.cornerRadius = cell.serialCircleImageView.frame.size.width/2;
    cell.serialCircleImageView.clipsToBounds = YES;
    cell.serialCircleImageView.layer.borderWidth = 1.0;
    cell.serialCircleImageView.layer.borderColor = [[UIColor colorWithRed:63.0/255.0 green:106.0/255.0 blue:128.0/255.0 alpha:1.0] CGColor];

    if ([self.conditionString isEqualToString:@"Event"]) {
        
        cell.bonusImageView.hidden = TRUE;
        cell.rankLabel.hidden = TRUE;
        cell.calenderImageView.hidden = TRUE;
        cell.calenderLabel.hidden= TRUE;
        
        cell.rankLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"rank"]];
        cell.toppernameLabel.text  = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"name"]];
        cell.topperidLabel.text  = [NSString stringWithFormat:@"(%@)",[topperDictionary valueForKeyPath:@"playerid"]];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"score"]];
    }
    else {
        
        cell.bonusImageView.hidden = FALSE;
        cell.rankLabel.hidden = FALSE;
        cell.calenderImageView.hidden = FALSE;
        cell.calenderLabel.hidden= FALSE;
        
        cell.rankLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"rank"]];
        cell.toppernameLabel.text  = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"name"]];
        cell.topperidLabel.text  = [NSString stringWithFormat:@"(%@)",[topperDictionary valueForKeyPath:@"playerid"]];
        cell.scoreLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"score"]];
        cell.BonusLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"bonus_point"]];
        cell.calenderLabel.text = [NSString stringWithFormat:@"%@",[topperDictionary valueForKeyPath:@"dateadded"]];
    }
    
    if (indexPath.row == topperArray.count - 1) {
        
        cell.rulesDashImageView.hidden = TRUE;
    }
    else {
        
        cell.rulesDashImageView.hidden = FALSE;
    }

    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}


-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton {
    
    objMenu.hidden = TRUE;
    
    RulesViewController *objRules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objRules animated:YES];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Profile";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"],[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200){
                
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
