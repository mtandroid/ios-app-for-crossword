//
//  PlayerViewController.h
//  Mashithantu
//
//  Created by Shyam on 26/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MBCircularProgressBarView.h"
#import "MMMaterialDesignSpinner.h"
#import "Menu.h"

@interface PlayerViewController : UIViewController {
    
    IBOutlet UILabel * nameLabel,*scoreLabel,*totalplayedLabel,*createdLabel,*scored100Label,*rank1bonusLabel,*bonusLabel,*otherbonusLabel,*genderLabel,*agegroupLabel,*publicprofileLabel,*locationLabel,*playernameLabel,*progressLabel;
    IBOutlet UIButton * playerprofileButton,*performanceButton,*homeButton,*menuButton;
    IBOutlet UIScrollView * playerScroll;
    IBOutlet UIView *playerView;
    IBOutlet UITableView *performanceTable;
    IBOutlet MBCircularProgressBarView *Progresstracker;
    Reachability *reach;
    WebService *webService;
    MMMaterialDesignSpinner *spinner;
    Menu *objMenu;
    UIActivityIndicatorView * activityIndicator;
    NSMutableArray * performanceArray;
}

@property(nonatomic,retain) NSString * playeridString;

-(IBAction)playerprofileButton:(id)sender;
-(IBAction)performanceButton:(id)sender;
-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;

@end
