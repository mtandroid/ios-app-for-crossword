//
//  LoginViewController.m
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "LoginViewController.h"
#import "MashiLoginViewController.h"
#import "AvailabilityViewController.h"
#import "HomeViewController.h"
#import "VerificationViewController.h"
#import <Google/Analytics.h>
#import "Constants.h"
#import "UIView+Toast.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;

    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    webService = [WebService api];
    
    loginScroll.contentSize = CGSizeMake(320, 568);
}

-(void)viewWillAppear:(BOOL)animated {
  
    [FBSDKAccessToken setCurrentAccessToken:nil];
    [[GIDSignIn sharedInstance] signOut];

    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"Login"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(IBAction)fbloginButton:(id)sender {
    
    if ([FBSDKAccessToken currentAccessToken]) {
        
        if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
            
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth = 5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            [self performSelector:@selector(socialLoginApi) withObject:spinner afterDelay:0.0];
        }
        else {
            
            [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        }
    }
    else {
        
        loginManager = [[FBSDKLoginManager alloc] init];
        
        [loginManager logInWithReadPermissions:@[@"public_profile",@"email"]fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if(!error) {
                
                NSMutableDictionary* parameters = [NSMutableDictionary dictionary];[parameters setValue:@"id,first_name,last_name,email" forKey:@"fields"];

                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    
                    if (!error) {
                        
                        emailString = result[@"email"];
                        
                        firstnameString = [result[@"first_name"] stringByReplacingOccurrencesOfString:@" " withString:@""];
                        
                        lastnameString = result[@"last_name"];
                        
                        useridString =[result objectForKey:@"id"];
                        
                        NSLog(@"emailString : %@", emailString);
                        if (emailString == NULL) {
                            
                            [self.view makeToast:@"Please login with your email id" duration:3.0 position:CSToastPositionTop];
                        }
                        else {
                            
                            if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
                                
                                [spinner stopAnimating];
                                //Activityindicator for loading the history
                                spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
                                spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
                                spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
                                spinner.lineWidth = 5.0;
                                spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
                                [self.view addSubview:spinner];
                                [spinner startAnimating];
                                [self performSelector:@selector(socialLoginApi) withObject:spinner afterDelay:0.0];
                            }
                            else {
                                
                                [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
                            }
                        }
                    }
                }];
            }
        }];
    }
}

-(IBAction)siteButton:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:API_BASE_URL]];
}

-(IBAction)guestButton:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"Guest" forKey:@"UserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    HomeViewController *objHome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:objHome animated:YES];
}

- (IBAction)googlePlusButtonTouchUpInside:(id)sender {
    
    [GIDSignIn sharedInstance].delegate=self;
    [GIDSignIn sharedInstance].uiDelegate=self;
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    
    //Perform any operations on signed in user here.
    useridString = user.userID;                  // For client-side use only!
    // NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *fullName = user.profile.name;
    NSArray *items = [fullName componentsSeparatedByString:@" "];   //take one array for splitting the string
    firstnameString =[[items objectAtIndex:0]stringByReplacingOccurrencesOfString:@" "withString:@""];
    lastnameString = [items objectAtIndex:1];

    emailString = user.profile.email;
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth = 5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        [self performSelector:@selector(socialLoginApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
    }
}

// Present a view that prompts the user to sign in with Google

- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    
    [self presentViewController:viewController animated:YES completion:nil];
    
}

// Dismiss the "Sign in with Google" view

- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)displayid {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=userid_select&ext_userid=%@&ext_first=%@&ext_last=%@&ext_email=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], useridString, firstnameString, lastnameString, emailString] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"USERIDAVAILABILITY:%@", resultDictionary);
        
        [spinner stopAnimating];
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
                        
            AvailabilityViewController *objAvailable = [self.storyboard instantiateViewControllerWithIdentifier:@"AvailabilityViewController"];
            objAvailable.availabilityArray = [resultDictionary objectForKey:@"idlist"];
            objAvailable.nameString = firstnameString;
            objAvailable.emailString = emailString;
            [self.navigationController pushViewController:objAvailable animated:YES];
        }
        else {
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
        }
    }];
}

- (void)socialLoginApi
{
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=userid_check&userid=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], emailString] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"USERRES:%@",resultDictionary);
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
            
            [self displayid];
        }
        else if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 201) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[resultDictionary objectForKey:@"player_id"] forKey:@"UserId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [self thirdApi];
        }
        else {
            
            [spinner stopAnimating];
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
        }
    }];
}

-(void)thirdApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=register3rd&deviceid=%@&userid=%@&name=%@&email=%@&pass=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], firstnameString, emailString, [[NSUserDefaults standardUserDefaults] objectForKey:@"MD5pass"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Munesan : %@",resultDictionary);
        
        [spinner stopAnimating];
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"AppInside" forKey:@"AppStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            HomeViewController *objhome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:objhome animated:YES];
        }
        else if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 231) {
            
            VerificationViewController * objVerify = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
            objVerify.useridString =  [NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"userid"]];
            objVerify.idString = [NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"id"]];
            [self.navigationController pushViewController:objVerify animated:YES];
        }
        else {
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
        }
    }];
}


-(IBAction)mashiButton:(id)sender {
    
    MashiLoginViewController *objLogin= [self.storyboard instantiateViewControllerWithIdentifier:@"MashiLoginViewController"];
    [self.navigationController pushViewController:objLogin animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
