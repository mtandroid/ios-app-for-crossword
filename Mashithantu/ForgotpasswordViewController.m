//
//  ForgotpasswordViewController.m
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ForgotpasswordViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"

@interface ForgotpasswordViewController ()

@end

@implementation ForgotpasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden= YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    forgotScroll.contentSize = CGSizeMake(320, 568);
    
    emailText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Your E-mail"attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];
    
    sendOTPButton.layer.borderWidth = 1.5;
    sendOTPButton.layer.cornerRadius = 3.0;
    sendOTPButton.layer.borderColor = [[UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0] CGColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

- (void)dismissKeyboard {
    
    [emailText resignFirstResponder];
}

-(IBAction)sendOTPButton:(id)sender {
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        if (emailText.text.length == 0) {
            
            [self.view makeToast:EmailEmpty duration:3.0 position:CSToastPositionTop];
            [emailText becomeFirstResponder];
        }
        else {
            
            sendOTPButton.enabled = NO;
            
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth =5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            
            [self performSelector:@selector(forgotApi) withObject:spinner afterDelay:0.0];
        }
    }
    
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)forgotApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=forgot_pass&userid=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [emailText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
    
        NSLog(@"Forgot Response%@",resultDictionary);
        
         if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200) {
        
             UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Success"
                                                                    message:[resultDictionary valueForKeyPath:@"errorstring"]
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
             [successAlert show];
         }
         else {
             
             if ([resultDictionary objectForKey:@"errorstring"]) {
                 
                 [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
             }
         }
        [spinner stopAnimating];
        sendOTPButton.enabled = YES;
    }];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [emailText resignFirstResponder];
    return YES;
}


-(IBAction)cancelButton:(id)sender {
    
   [self.navigationController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
