//
//  RegisterViewController.m
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "RegisterViewController.h"
#import "MashiLoginViewController.h"
#import "VerificationViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"

@interface RegisterViewController () 

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    registerScroll.contentSize = CGSizeMake(320, 830);
    
    arrCapElements = [[NSArray alloc] initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"a",@"b",@"c",@"d",@"e",@"f",@"g",@"h",@"i",@"j",@"k",@"l",@"m",@"n",@"o",@"p",@"q",@"r",@"s",@"t",@"u",@"v",@"w",@"x",@"y",@"z",@"A",@"B",@"C",@"D",@"E",@"F",@"G",@"H",@"I",@"J",@"K",@"L",@"M",@"N",@"O",@"P",@"Q",@"R",@"S",@"T",@"U",@"V",@"W",@"X",@"Y",@"Z", nil];
    
    firstnameText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter First name,eg:Cullen" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];
    lastnameText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Last Name" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];
    useridText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter user id,eg:puzzleking" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];
    emailidText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Email ID,eg:twilightsaga@transbets.com" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];;
    passwordText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter a Secret Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:78.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]}];

    createButton.layer.borderWidth = 1.5;
    createButton.layer.borderColor = [[UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0] CGColor];
    createButton.layer.cornerRadius = 3.0;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    [self load_captcha];
    
    captchaView.hidden = TRUE;
    
    verifyButton.layer.borderWidth = 1.0;
    verifyButton.layer.borderColor = [[UIColor colorWithRed:62.0/255.0 green:119.0/255.0 blue:116.0/255.0 alpha:1.0] CGColor];
    
    captchaTextfield .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Above String Here.." attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:loginLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 24)];
    [loginLabel setAttributedText:clueString];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
}

-(IBAction)createButton:(id)sender {
    
      if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
          
          NSString *emailRegex = @"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,4})$";
          
          NSPredicate* emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
          NSString *subjectString = emailidText.text;
          NSString *nameRegex = @"[a-zA-Z][a-zA-Z ]*";
          NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
          NSString * subjectnameString1 = firstnameText.text;
          NSString * subjectnameString2 = lastnameText.text;
          
          if (firstnameText.text.length == 0) {
              
              [self.view makeToast:FirstNameEmpty duration:3.0 position:CSToastPositionTop];
              [firstnameText becomeFirstResponder];
          }
          
          else if ([nameTest evaluateWithObject:subjectnameString1] != YES) {
              
              [self.view makeToast:NameError duration:3.0 position:CSToastPositionTop];
              [firstnameText becomeFirstResponder];
          }
          
          else if (lastnameText.text.length == 0) {
              
              [self.view makeToast:LasttNameEmpty duration:3.0 position:CSToastPositionTop];
              [lastnameText becomeFirstResponder];
          }
          
          else if ([nameTest evaluateWithObject:subjectnameString2] != YES) {
              
              [self.view makeToast:NameError duration:3.0 position:CSToastPositionCenter];
              [lastnameText becomeFirstResponder];
          }
         else if (emailidText.text.length == 0) {
              
              [self.view makeToast:EmailEmpty duration:3.0 position:CSToastPositionTop];
              [emailidText becomeFirstResponder];
          }
          
          else if ([emailTest evaluateWithObject:subjectString] != YES) {
              
              [self.view makeToast:EmailError  duration:3.0 position:CSToastPositionTop];
              [emailidText becomeFirstResponder];
          }
          
          else if (passwordText.text.length == 0) {
              
              [self.view makeToast:passwordEmpty duration:3.0 position:CSToastPositionTop];
              [passwordText becomeFirstResponder];
          }
          
          else {
              [self dismissKeyboard];
              createButton.enabled = NO;
              firstnameText.enabled = FALSE;
              lastnameText.enabled = FALSE;
              emailidText.enabled = FALSE;
              useridText.enabled = FALSE;
              passwordText.enabled = FALSE;
              captchaView.hidden = FALSE;
              [registerScroll setContentOffset:CGPointZero animated:YES];
//              registerScroll.scrollEnabled = FALSE;
//
          }
      }

      else {
          
          [self.view makeToast:@"No Network" duration:3.0 position:CSToastPositionTop];
          return;
      }
       
}

- (void)dismissKeyboard
{
    [firstnameText resignFirstResponder];
    [lastnameText resignFirstResponder];
    [emailidText resignFirstResponder];
    [useridText resignFirstResponder];
    [passwordText resignFirstResponder];
    [captchaTextfield resignFirstResponder];
}


-(void)useridApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=userid_check&userid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"],[useridText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Available Response%@",resultDictionary);
        
        if([[resultDictionary valueForKeyPath:@"errorcode"] intValue] == 200) {
            
            [useridText resignFirstResponder];
            [emailidText becomeFirstResponder];

        }
        else {
            
            [self.view makeToast:[resultDictionary valueForKeyPath:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
            [useridText becomeFirstResponder];
            
            
        }
        
    }];
    
}

-(void)registrationApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=register&deviceid=%@&userid=%@&name=%@&email=%@&pass=%@&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"],[useridText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[firstnameText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[emailidText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[passwordText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Registration Response%@",resultDictionary);
        
        if([[resultDictionary valueForKeyPath:@"errorcode"] intValue] == 200) {
            
            [[NSUserDefaults standardUserDefaults]setObject:[resultDictionary valueForKeyPath:@"id"] forKey:@"Id"];
            [[NSUserDefaults standardUserDefaults]synchronize];
            
        UIAlertView *successAlert = [[UIAlertView alloc] initWithTitle:@"Registration Success"message:[resultDictionary valueForKeyPath:@"errorstring"]delegate:self cancelButtonTitle:@"OK"otherButtonTitles:nil];
            
            [successAlert show];
            
        }
        else {
            
            [self.view makeToast:[resultDictionary valueForKeyPath:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
        }
        [spinner stopAnimating];
    }];
}

-(void)load_captcha{
    
    @try {
        
      //  CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
      //  CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
      //  CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
       // CaptchaLabel.backgroundColor = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        //Captcha_label.textColor=[UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        NSUInteger elmt1,elmt2,elmt3,elmt4,elmt5,elmt6;
        elmt1 = arc4random() % [arrCapElements count];
        elmt2= arc4random() % [arrCapElements count];
        elmt3 = arc4random() % [arrCapElements count];
        elmt4 = arc4random() % [arrCapElements count];
        elmt5 = arc4random() % [arrCapElements count];
        elmt6 = arc4random() % [arrCapElements count];
        
        NSString *Captcha_string = [NSString stringWithFormat:@"%@%@%@%@%@%@",arrCapElements[elmt1-1],arrCapElements[elmt2-1],arrCapElements[elmt3-1],arrCapElements[elmt4-1],arrCapElements[elmt5-1],arrCapElements[elmt6-1]];
        //NSLog(@" Captcha String : %@",Captcha_string);
        captchaLabel.text = Captcha_string;
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
}

- (IBAction)Reload_Action:(id)sender {
    
    [self load_captcha];
}

- (IBAction)verifyButton:(id)sender {
    
    NSLog(@"Here");
    NSLog(@"%@ = %@",captchaLabel.text,captchaTextfield.text);
    
    if(captchaTextfield.text.length==0){
        
    [self.view makeToast:@"Please Enter The Code Below" duration:3.0 position:CSToastPositionCenter];
    }
    
   else  if([captchaLabel.text isEqualToString: captchaTextfield.text]){
        [self.view endEditing:YES];
        captchaView.hidden = TRUE;
       [spinner stopAnimating];
       //Activityindicator for loading the history
       spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
       spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
       spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
       spinner.lineWidth =5.0;
       spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
       [self.view addSubview:spinner];
       [spinner startAnimating];
       [self performSelector:@selector(registrationApi) withObject:spinner afterDelay:0.0];
    }
    else{
        [self.view makeToast:@"Incorrect" duration:3.0 position:CSToastPositionCenter];
    }
}

-(IBAction)showhideButton:(id)sender {
    
    if(passwordText.secureTextEntry == TRUE) {
        
        passwordText.secureTextEntry = FALSE;
        [showhideButton setSelected:NO];
        [showhideButton setTitle:@"Hide" forState:UIControlStateNormal];
        
    }
    
    else {

        [showhideButton setTitle:@"Show" forState:UIControlStateNormal];
        passwordText.secureTextEntry = TRUE;
        [showhideButton setSelected:YES];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  
    VerificationViewController *objVerify = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationViewController"];
    
    objVerify.useridString = useridText.text;
    [self.navigationController pushViewController:objVerify animated:YES];
}


-(IBAction)loginhereButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == firstnameText) {
        
        [firstnameText resignFirstResponder];
        [lastnameText becomeFirstResponder];
    }
    else if (textField==lastnameText) {
        
        [lastnameText resignFirstResponder];
        [useridText becomeFirstResponder];
    }
    else if (textField==useridText) {
        
        if (useridText.text.length == 0) {
            
            [self.view makeToast:UserIdEmpty duration:3.0 position:CSToastPositionTop];
            [useridText becomeFirstResponder];
        }
        
        else if (useridText.text.length < 6) {
            
            [self.view makeToast:@"User Id must be of minimum 6 characters" duration:3.0 position:CSToastPositionTop];
            [useridText becomeFirstResponder];
        }
        else if (useridText.text.length > 50) {
            
            [self.view makeToast:@"User Id must not exceed 100 characters" duration:3.0 position:CSToastPositionTop];
            [useridText becomeFirstResponder];
        }
        
        else {
            [self useridApi];
        }
    }
    else if (textField==emailidText) {
        
        [emailidText resignFirstResponder];
        [passwordText becomeFirstResponder];
    }
    else if (textField==passwordText) {
        
        [passwordText resignFirstResponder];
    }
    else if (textField==captchaTextfield) {
        
        [captchaTextfield resignFirstResponder];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
