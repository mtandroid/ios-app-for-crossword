//
//  ChangePasswordViewController.m
//  Mashithantu
//
//  Created by Nidhin Baby on 22/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"

@interface ChangePasswordViewController ()

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    changeButton.layer.cornerRadius = 5.0;
    changeButton.layer.borderWidth = 2.0;
    changeButton.layer.borderColor = [[UIColor colorWithRed:82.0/255.0 green:158.0/255.0 blue:166.0/255.0 alpha:1]CGColor];
    
    oldpasswordText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Old Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    newpasswordText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter New Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    retypepasswordText .attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"ReType Password" attributes:@{NSForegroundColorAttributeName:[UIColor colorWithRed:93.0/255.0 green:115.0/255.0 blue:128.0/255.0 alpha:1.0]}];
    [oldpasswordText becomeFirstResponder];
    
    passwordScroll.contentSize = CGSizeMake(320, 568);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];

    // Do any additional setup after loading the view.
    webservice = [WebService api];
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    
}
- (void)dismissKeyboard
{
    [oldpasswordText resignFirstResponder];
    [newpasswordText resignFirstResponder];
    [retypepasswordText resignFirstResponder];
}

-(IBAction)changeButton:(id)sender {
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        if(oldpasswordText.text.length == 0) {
            
            [self.view makeToast:@"Please Enter Your Passsword" duration:3.0 position:CSToastPositionCenter];
        }
        
        else if (newpasswordText.text.length == 0) {
            [self.view makeToast:@"Please Enter New Password" duration:3.0 position:CSToastPositionCenter];
        }
        
        else if (newpasswordText.text.length == 0) {
            [self.view makeToast:@"Please Retype New Password" duration:3.0 position:CSToastPositionCenter];
        }

        
        else if (![newpasswordText.text isEqualToString:retypepasswordText.text] ) {
            [self.view makeToast:@"Passwords Doesn't Match" duration:3.0 position:CSToastPositionCenter];
        }
        
        else {
        
            changeButton.enabled = NO;
            
            [activityIndicator stopAnimating];
            //Activityindicator for loading the history
            activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            activityIndicator.color = [UIColor whiteColor];
            activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:activityIndicator];
            [activityIndicator startAnimating];
        [self performSelector:@selector(changeApi) withObject:nil afterDelay:0.0];
            }
        }
    else {
        [self.view makeToast:@"No network available" duration:3.0 position:CSToastPositionCenter];
    }
}


-(void)changeApi {
    
    [webservice getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=change_pass&deviceid=%@&userid=%@&pass=%@&newpass=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"],[[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"],[oldpasswordText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[retypepasswordText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Login Response %@",resultDictionary);
        
      if([[resultDictionary valueForKeyPath:@"errorcode"]intValue] == 200) {
         
          UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Password Changed" message:[resultDictionary valueForKeyPath:@"errorstring"]delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
           [alert show];
          [self.navigationController popViewControllerAnimated:YES];
      }
          else {
              
          [self.view makeToast:[resultDictionary valueForKeyPath:@"errorstring"] duration:3.0 position:CSToastPositionCenter];
          [activityIndicator stopAnimating];
          }
    }];
    changeButton.enabled = YES;
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == oldpasswordText) {
        
        [oldpasswordText resignFirstResponder];
        [newpasswordText becomeFirstResponder];
    }
    else if (textField==newpasswordText) {
        [newpasswordText resignFirstResponder];
        [retypepasswordText becomeFirstResponder];
    }
    else {
        [retypepasswordText resignFirstResponder];
    }
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
        if (textField == oldpasswordText) {
        oldimageview.backgroundColor = [UIColor colorWithRed:181.0/255.0 green:55.0/255.0 blue:94.0/255.0 alpha:1.0];
        newimageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
        retypeimageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }
    else if (textField == newpasswordText) {
        newimageView.backgroundColor = [UIColor colorWithRed:181.0/255.0 green:55.0/255.0 blue:94.0/255.0 alpha:1.0];
        oldimageview.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
        retypeimageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }
    else  {
        retypeimageView.backgroundColor = [UIColor colorWithRed:181.0/255.0 green:55.0/255.0 blue:94.0/255.0 alpha:1.0];
        oldimageview.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
        newimageView.backgroundColor = [UIColor colorWithRed:58.0/255.0 green:108.0/255.0 blue:117.0/255.0 alpha:1.0];
    }

}


-(IBAction)cancelButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
