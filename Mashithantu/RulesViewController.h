//
//  RulesViewController.h
//  Mashithantu
//
//  Created by Nidhin Baby on 24/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface RulesViewController : UIViewController
{
    IBOutlet UILabel *firstprizeLabel,*secondprizeLabel,*thirdprizeLabel;
    IBOutlet UIButton *homeButton,*menuButton;
    IBOutlet UITableView *rulesTable,*prizesTable;
    IBOutlet UIView *footerView;
    WebService *webService;
    Reachability *reach;
    NSMutableArray *rulesMutableArray,*prizesMutableArray,*consolationArray;
    MMMaterialDesignSpinner *spinner;
}

-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;

@end
