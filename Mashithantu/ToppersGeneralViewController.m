//
//  ToppersGeneralViewController.m
//  Mashithantu
//
//  Created by Shyam on 26/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ToppersGeneralViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "CustomCell.h"
#import "SVPullToRefresh.h"
#import "PlayerViewController.h"
#import "ProfileViewController.h"
#import "RulesViewController.h"

@interface ToppersGeneralViewController ()

@end

@implementation ToppersGeneralViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    objMenu = [[Menu alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        [objMenu .tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [self toppersApi];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)toppersApi {
    
    topperpage = 0;
    
    eventtopperlistArray = [[NSMutableArray alloc]init];
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(toppergeneralApi) withObject:spinner afterDelay:0.0];
    
    __weak ToppersGeneralViewController *weakSelf = self;
    
    [toppersTable addInfiniteScrollingWithActionHandler:^{
        
        [weakSelf toppergeneralApi];
    }];
}


-(void)toppergeneralApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=event_topper_list&pagesize=10&page=%d", topperpage] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Toppergeneraldictionary : %@",resultDictionary);
        
        NSArray* toppergeneralArray = [resultDictionary objectForKey:@"event_topper_list"];
       
        if (toppergeneralArray.count == 0) {
        
          [self.view makeToast:@"End Of the List" duration:3.0 position:CSToastPositionTop];
        }
        else {
            
            topperpage = topperpage + 1;
            
            [eventtopperlistArray addObjectsFromArray:toppergeneralArray];
        }
        [toppersTable reloadData];
        [spinner stopAnimating];
        [toppersTable.infiniteScrollingView stopAnimating];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return  [eventtopperlistArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {

    NSDictionary * toppergeneraldictionary = [eventtopperlistArray objectAtIndex:section];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 40)];
    [headerView setBackgroundColor:[UIColor colorWithRed:61.0/255.0 green:148.0/255.0 blue:177.0/255.0 alpha:0.14]];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, [[UIScreen mainScreen] bounds].size.width - 20, 40)];
    [headerLabel setFont:[UIFont fontWithName:@"Nunito-bold" size:12]];
    [headerLabel setTextColor:[UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:0.75]];
    [headerLabel setText:[toppergeneraldictionary valueForKeyPath:@"event"]];
    [headerView addSubview:headerLabel];
    
//    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
//    [btn setFrame:CGRectMake(0, 0, tableView.frame.size.width, 80)];
//    [btn setTag:section+1];
//    [view addSubview:btn];
//    [btn addTarget:self action:@selector(sectionTapped:) forControlEvents:UIControlEventTouchDown];

    return headerView;
}


//- (void)sectionTapped:(UIButton*)btn {
//    
//    tag = btn.tag - 1;
//    if (buttonSelected == 1) {
//        height = 1.0;
//        buttonSelected = 0;
//    }
//    else if (buttonSelected == 0) {
//        height = 85.0;
//        buttonSelected = 1;
//    }
//    [toppersgeneralTableView reloadData];
//}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   return [[[eventtopperlistArray objectAtIndex:section] objectForKey:@"toppers"] count];
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if(indexPath.section == tag) {
//    
//        return height;
//    }
//    else {
//        return 1.0;
//    }
// 
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    cell.serialCircleImageView.layer.cornerRadius = cell.serialCircleImageView.frame.size.width/2;
    cell.serialCircleImageView.clipsToBounds = YES;
    cell.serialCircleImageView.layer.borderWidth = 1.0;
    cell.serialCircleImageView.layer.borderColor = [[UIColor colorWithRed:61.0/255.0 green:148.0/255.0 blue:177.0/255.0 alpha:1.0] CGColor];
    
    NSDictionary* newdict = [eventtopperlistArray objectAtIndex:indexPath.section];
    
    cell.rankLabel.text = [NSString stringWithFormat:@"%@",[[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"rank"]];
    cell.toppernameLabel.text = [NSString stringWithFormat:@"%@",[[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"name"]];
    cell.topperidLabel.text = [NSString stringWithFormat:@"(%@)", [[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"playerid"]];
    cell.scoreLabel.text = [NSString stringWithFormat:@"%@", [[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"score"]];
    cell.BonusLabel.text = [NSString stringWithFormat:@"%@", [[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"bonus_point"]];
    cell.calenderLabel.text = [NSString stringWithFormat:@"%@", [[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"dateadded"]];
    
    if ([cell.BonusLabel.text isEqualToString:@""]) {
        
        cell.giftImageView.hidden = TRUE;
    }
    else {
        
        cell.giftImageView.hidden = FALSE;
    }
    
    if ([cell.calenderLabel.text isEqualToString:@""]) {
        
        cell.calendarImageView.hidden = TRUE;
    }
    else {
        
        cell.calendarImageView.hidden = FALSE;
    }
    
    if( indexPath.row == [[[eventtopperlistArray objectAtIndex:indexPath.section] objectForKey:@"toppers"] count] - 1) {
        
        cell.rulesDashImageView.hidden = TRUE;
    }
    else {
        
        cell.rulesDashImageView.hidden = FALSE;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary* newdict = [eventtopperlistArray objectAtIndex:indexPath.section];
    
    PlayerViewController * objplayer = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayerViewController"];
    objplayer.playeridString = [NSString stringWithFormat:@"%@", [[[newdict objectForKey:@"toppers"] objectAtIndex:indexPath.row] valueForKey:@"playerid"]];
    [self.navigationController pushViewController:objplayer animated:YES];
}

-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton {
    
    objMenu.hidden = TRUE;
    
    RulesViewController *objrules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objrules animated:YES];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Profile";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200){
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
