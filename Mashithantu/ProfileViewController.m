//
//  ProfileViewController.m
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ProfileViewController.h"
#import "RulesViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "CustomCell.h"
#import "SVPullToRefresh.h"
#import "ToppersViewController.h"
#import "ChangePasswordViewController.h"
#import "Menu.h"
#import "MMMaterialDesignSpinner.h"
#import <Google/Analytics.h>
#import "PlayViewController.h"

@interface ProfileViewController () {

    Menu *objMenu;
    MMMaterialDesignSpinner *spinner;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    objMenu = [[Menu alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        
        [objMenu.tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(rulesButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    appversionLabel.text = [NSString stringWithFormat:@"Mashithantu crossword ver %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    
    popupView.hidden = TRUE;
    
    helpView.layer.borderWidth = 0.8;
    helpView.layer.borderColor = [[UIColor colorWithRed:59.0/255.0 green:104.0/255.0 blue:105.0/255.0 alpha:0.2]CGColor];
    
    yesButton.layer.borderWidth = 1.0;
    yesButton.layer.borderColor = [[UIColor greenColor] CGColor];
    yesButton.layer.cornerRadius = 5.0;
    
    rateView.layer.borderWidth = 0.8;
    rateView.layer.borderColor = [[UIColor colorWithRed:59.0/255.0 green:104.0/255.0 blue:105.0/255.0 alpha:0.2]CGColor];
    rateButton.layer.borderWidth = 1.5;
    rateButton.layer.borderColor = [[UIColor colorWithRed:104.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:0.8]CGColor];
    rateButton.layer.cornerRadius = 3.0;
    
    if ([self.screenString isEqualToString:@"Settings"]) {
        
        profileScroll.contentSize = CGSizeMake(320, 1160);
        
        settingsView.hidden = FALSE;
        accountView.hidden = TRUE;
        mygamesView.hidden = TRUE;
        
        [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
        [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
        
        if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
            
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth =5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            
            [self performSelector:@selector(settingsApi) withObject:spinner afterDelay:0.0];
        }
        else {
            
            [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
            return;
        }
    }
    else {
        
        profileScroll.contentSize = CGSizeMake(320, 550);
        
        accountView.hidden = FALSE;
        settingsView.hidden = TRUE;
        mygamesView.hidden = TRUE;

        [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
        [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
        
        if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth = 5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            
            [self performSelector:@selector(accountApi) withObject:spinner afterDelay:0.0];
        }
        else {
        
            [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
            return;
        }
    }
}

-(IBAction)tapButton:(id)sender {
    
    [self dismissKeyboard];
}

- (void)dismissKeyboard
{
    [displayText resignFirstResponder];
    [genderText resignFirstResponder];
    [publicprofileText resignFirstResponder];
    [locationText resignFirstResponder];
    [addressText resignFirstResponder];
    [prizelocationText resignFirstResponder];
    [pinText resignFirstResponder];
    [phonenumber1Text resignFirstResponder];
    [phonenumber2Text resignFirstResponder];
    
    helpView.hidden = TRUE;
    rateView.hidden = TRUE;
}

-(void)settingsApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=myaccount_settings_get&player_id=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"settingsDictionary : %@",resultDictionary);
        
        displayText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"displayname"]];
        genderText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"gender"]];
        agegroupLabel.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"age"]];
        publicprofileText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"publicprofile"]];
        locationText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"displaylocation"]];
        emailLabel.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"email"]];
        addressText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"prizedeliveryaddr"]];
        prizelocationText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"prizedeliverydistrict"]];
        pinText.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"prizedeliverypin"]];
        phonenumber1Text.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"prizedeliveryph1"]];
        phonenumber2Text.text = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"prizedeliveryph2"]];
        stateString = [NSString stringWithFormat:@"%@",[resultDictionary valueForKeyPath:@"crosswordreminder"]];
            
        if ([stateString isEqualToString:@"TRUE"]) {
            
            [setreminderButton setSelected:YES];
        }
        else {
            
            [setreminderButton setSelected:NO];
        }
        [spinner stopAnimating];
    }];
}

-(void)accountApi {
    
    accountsArray = [[NSMutableArray alloc] init];
    
     [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=myaccount_acc&player_id=%@&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
         
         
        NSLog(@"AccountDictionary : %@",resultDictionary);

         NSArray *sectionarray = [resultDictionary objectForKey:@"sections"];
         NSArray *resultArray = [resultDictionary  objectForKey:@"EventDetails"];
         
         if (resultArray.count == 0) {
             
         }
         else {
             
             [accountsArray addObjectsFromArray:[resultArray subarrayWithRange:NSMakeRange(0, 10)]];
         }
         
         if (sectionarray.count != 0) {
             
             totalscoreaccountLabel.text = [[sectionarray objectAtIndex:0] valueForKeyPath:@"TotalScore"];
             bookpointaccountLabel.text = [[sectionarray objectAtIndex:0] valueForKeyPath:@"BookPoints"];
             totalaccountplayedLabel.text = [[sectionarray objectAtIndex:0] valueForKeyPath:@"TotalCrosswordPlayed"];
             totalaccountcreatedLabel.text = [[sectionarray objectAtIndex:0] valueForKeyPath:@"TotalCrosswordPublished"];
             progressLabel.text = totalaccountplayedLabel.text;
             
             [Progresstracker setValue:[ totalaccountplayedLabel.text floatValue] animateWithDuration:5.0];
             [Progresstracker setMaxValue:[[[sectionarray objectAtIndex:0] valueForKeyPath:@"TotalCrosswords"] floatValue]];
             [Progresstracker setProgressColor:[UIColor greenColor]];
             [Progresstracker setProgressStrokeColor:[UIColor greenColor]];
             [Progresstracker setProgressLineWidth:2.0];
             [Progresstracker setProgressAngle:100];
             [Progresstracker setProgressRotationAngle:50];
         }
         [accountTable reloadData];
         [spinner stopAnimating];
     }];
}


-(IBAction)settingsButton:(id)sender {
    
    [self dismissKeyboard];
    
    settingsView.hidden = FALSE;
    accountView.hidden = TRUE;
    mygamesView.hidden = TRUE;

    profileScroll.contentSize = CGSizeMake(320, 1160);
    profileScroll.scrollEnabled = YES;
    
    [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(settingsApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(IBAction)accountButton:(id)sender {
    
    [self dismissKeyboard];
    
    profileScroll.contentSize = CGSizeMake(320, 550);
    profileScroll.scrollEnabled = YES;

    [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    
    accountView.hidden = FALSE;
    settingsView.hidden = TRUE;
    mygamesView.hidden = TRUE;
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(accountApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)rulesButton {
    
    objMenu.hidden = TRUE;
    
    RulesViewController *objRules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objRules animated:YES];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    profileScroll.contentSize = CGSizeMake(320, 550);
    profileScroll.scrollEnabled = YES;
    
    [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    
    accountView.hidden = FALSE;
    settingsView.hidden = TRUE;
    mygamesView.hidden = TRUE;
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(accountApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    settingsView.hidden = FALSE;
    accountView.hidden = TRUE;
    mygamesView.hidden = TRUE;
    
    profileScroll.contentSize = CGSizeMake(320, 1160);
    profileScroll.scrollEnabled = YES;
    
    [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(settingsApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

-(void)exitButton {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200){
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}


-(IBAction)gamesButton:(id)sender {

    profileScroll.contentSize = CGSizeMake(320, [[UIScreen mainScreen] bounds].size.height - 64);
    profileScroll.scrollEnabled = NO;
    
    [accountButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [settingsButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [gamesButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateNormal];
 
    settingsView.hidden = TRUE;
    accountView.hidden = TRUE;
    mygamesView.hidden = FALSE;
    
    helpView.hidden = TRUE;
    rateView.hidden = TRUE;
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [self performSelector:@selector(gamewithpageApi) withObject:nil afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}


-(void)gamewithpageApi {
    
    historypage = 0;
    
    historyArray = [[NSMutableArray alloc] init];
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(gameApi) withObject:spinner afterDelay:0.0];
    
    __weak ProfileViewController *weakSelf = self;
    
    [mygamesTable addInfiniteScrollingWithActionHandler:^{
        [weakSelf gameApi];
    }];
}

-(void)gameApi {
        
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=playhistory&player_id=%@&page=%d", [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], historypage] completion:^(NSDictionary *resultDictionary, NSError *error)
    {
            
        NSLog(@"GamePlayDictionary : %@",resultDictionary);
           
        NSArray *resultArray = [resultDictionary objectForKey:@"PlayDetails"];
            
        if (resultArray.count == 0) {
                
            [self.view makeToast:@"No more list items" duration:3.0 position:CSToastPositionBottom];
        }
        else {
                
            historypage = historypage + 2;
            [historyArray addObjectsFromArray:resultArray];
        }
        [mygamesTable reloadData];
        [mygamesTable.infiniteScrollingView stopAnimating];
        [spinner stopAnimating];
    }];
}

-(IBAction)setreminderButton:(id)sender {
    
    if ([stateString isEqualToString:@"FALSE"]) {
        
        stateString = @"TRUE";
        [setreminderButton setSelected:YES];
    }
    else {
        
        stateString = @"FALSE";
        [setreminderButton setSelected:NO];
    }
    
    if(displayText.text.length == 0) {
        
        [self.view makeToast:@"Please enter name" duration:3.0 position:CSToastPositionTop];
        [displayText becomeFirstResponder];
    }
    else if (genderText.text.length==0) {
        
        [self.view makeToast:@"Please enter your gender" duration:3.0 position:CSToastPositionTop];
        [genderText becomeFirstResponder];
    }
    
    else if (publicprofileText.text.length==0) {
        
        [self.view makeToast:@"Please enter your profile url" duration:3.0 position:CSToastPositionTop];
        [publicprofileText becomeFirstResponder];
    }
    
    else if (locationText.text.length==0) {
        
        [self.view makeToast:@"Please enter your location" duration:3.0 position:CSToastPositionTop];
        [locationText becomeFirstResponder];
    }
    
    else if (addressText.text.length==0) {
        
        [self.view makeToast:@"Please enter your address" duration:3.0 position:CSToastPositionTop];
        
    }
    
    else if (prizelocationText.text.length==0) {
        
        [self.view makeToast:@"Please enter your prize Delivery Address" duration:3.0 position:CSToastPositionTop];
        [prizelocationText becomeFirstResponder];
    }
    
    else if (pinText.text.length==0) {
        
        [self.view makeToast:@"Please enter your pin" duration:3.0 position:CSToastPositionTop];
        [pinText becomeFirstResponder];
    }
    
    else if (phonenumber1Text.text.length==0) {
        
        [self.view makeToast:@"Please enter your phonenumber" duration:3.0 position:CSToastPositionTop];
        [phonenumber1Text becomeFirstResponder];
    }
    
    else {
        
        [displayText resignFirstResponder];
        [genderText resignFirstResponder];
        [publicprofileText resignFirstResponder];
        [locationText resignFirstResponder];
        [prizelocationText resignFirstResponder];
        [pinText resignFirstResponder];
        [phonenumber1Text resignFirstResponder];
        [phonenumber2Text resignFirstResponder];
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        [self performSelector:@selector(updatedetails) withObject:spinner afterDelay:0.0];
    }
}


-(IBAction)helpButton:(id)sender {
    
    if (helpView.hidden == TRUE) {
        
        CATransition *transition2 = [CATransition animation];
        transition2.duration = 0.3;
        transition2.type = kCATransitionFade;
        transition2.subtype = kCATransitionFromRight;
        [transition2 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [helpView.layer addAnimation:transition2 forKey:nil];
        
        helpView.hidden = FALSE;
    }
    else {
        
        CATransition *transition2 = [CATransition animation];
        transition2.duration = 0.3;
        transition2.type = kCATransitionFade;
        transition2.subtype = kCATransitionFromRight;
        [transition2 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [helpView.layer addAnimation:transition2 forKey:nil];
        
        helpView.hidden = TRUE;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
 }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == accountTable) {
        
        if (accountsArray.count == 0) {
            
            return 10;
        }
        else {
            
            return accountsArray.count;
        }
    }
    if (tableView == mygamesTable) {
        
        return historyArray.count;
    }
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    if (tableView == accountTable) {
        
        if (accountsArray.count == 0) {
       
            emptyArray = @[@"NA",@"NA",@"NA",@"NA",@"NA",@"NA",@"NA",@"NA",@"NA",@"NA"];
            cell.rankLabel.textColor = [UIColor colorWithRed:53.0/255.0 green:73.0/255.0 blue:86.0/255.0 alpha:1.0];
            cell.rankLabel.text = [emptyArray objectAtIndex:indexPath.row];
        }
        else {
            
            NSDictionary *accountDictionary = [accountsArray objectAtIndex:indexPath.row];
    
            cell.rankLabel.text = [NSString stringWithFormat:@"%@", [accountDictionary valueForKeyPath:@"rank"]];
    
            cell.eventLabel.text = [NSString stringWithFormat:@"%@", [accountDictionary valueForKeyPath:@"event"]];
        }
    }
    
    if (tableView == mygamesTable) {
        
        NSDictionary *playHistoryDictionary = [historyArray objectAtIndex:indexPath.row];
        
        NSMutableAttributedString *colouredString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ (%@)", [playHistoryDictionary valueForKeyPath:@"cw_id"],[playHistoryDictionary valueForKeyPath:@"topic"]]];
        [colouredString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:107.0/255.0 green:197.0/255.0 blue:200.0/255.0 alpha:1.0]range:NSMakeRange(0, [NSString stringWithFormat:@"%@",[playHistoryDictionary valueForKeyPath:@"cw_id"]].length)];
        cell.cwcLabel.attributedText  = colouredString;
        
        cell.playtypeLabel.text = [NSString stringWithFormat:@"(%@)",[playHistoryDictionary valueForKeyPath:@"topic"]];
        
        cell.scoreLabel.text  = [NSString stringWithFormat:@"%@",[playHistoryDictionary valueForKeyPath:@"score"]];
        
        cell.BonusLabel.text = [NSString stringWithFormat:@"%@",[playHistoryDictionary valueForKeyPath:@"bonus"]];
        
        cell.ranknowLabel.text = [NSString stringWithFormat:@"%@",[playHistoryDictionary valueForKeyPath:@"rank"]];
    
        cell.calenderLabel.text = [NSString stringWithFormat:@"%@",[playHistoryDictionary valueForKeyPath:@"dateplayed"]];
        
        cell.rateview.value =[[playHistoryDictionary valueForKeyPath:@"rating"] floatValue];
        
        cell.ratedLabel.text = [NSString stringWithFormat:@"%d",[[playHistoryDictionary valueForKeyPath:@"rating"]intValue]];
        cell.rateview.tintColor = [UIColor orangeColor];
        
        cell.playButton.tag = indexPath.row;
        [cell.playButton addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.cellratebutton.tag = indexPath.row;
        [cell.cellratebutton addTarget:self action:@selector(rateButtonClicked:) forControlEvents:UIControlEventTouchUpInside];

        if ([[playHistoryDictionary valueForKeyPath:@"allow_rating"]isEqualToString:@"1"]) {
            
            cell.cellratebutton.hidden = FALSE;
            cell.rateview.enabled = NO;
            cell.rateImageView.image = [UIImage imageNamed:@"starrate.png"];
        }
        else {
            
            cell.cellratebutton.hidden = TRUE;
            cell.rateview.enabled = NO;
            cell.rateImageView.image = [UIImage imageNamed:@"rate.png"];
        }
        
        if ([[playHistoryDictionary valueForKeyPath:@"allow_play"] isEqualToString:@"1"]) {
            
            cell.playButton.hidden = FALSE;
        }
        else {
            
            cell.playButton.hidden = TRUE;
        }
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == accountTable) {
        
        if (accountsArray.count == 0) {
            
        }
        else {
            
            NSDictionary * accountDictionary = [accountsArray objectAtIndex:indexPath.row];
            
            ToppersViewController *objTopper = [self.storyboard instantiateViewControllerWithIdentifier:@"ToppersViewController"];
            objTopper.eventString  = [NSString stringWithFormat:@"%@", [accountDictionary valueForKeyPath:@"event"]];
            objTopper.conditionString = @"Event";
            [self.navigationController pushViewController:objTopper animated:YES];
        }
    }
    if (tableView == mygamesTable) {
        
        NSDictionary * playHistoryDictionary = [historyArray objectAtIndex:indexPath.row];
        
        ToppersViewController * objtopper = [self.storyboard instantiateViewControllerWithIdentifier:@"ToppersViewController"];
        objtopper.eventString  = [NSString stringWithFormat:@"%@", [playHistoryDictionary valueForKeyPath:@"cw_id"]];
        objtopper.conditionString = @"Cross";
        [self.navigationController pushViewController:objtopper animated:YES];
    }
}


-(void)playButtonClicked:(UIButton*)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFade;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [popupView.layer addAnimation:transition forKey:nil];
    popupView.hidden = FALSE;
    
    NSDictionary * playdictionary = [historyArray objectAtIndex:sender.tag];
    
    NSMutableAttributedString *colouredString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"topic:(%@)", [playdictionary valueForKeyPath:@"topic"]]];
    [colouredString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, 6)];
    topicLabel.attributedText = colouredString;
    
    cwcLabel.text = [NSString stringWithFormat:@"%@", [playdictionary valueForKeyPath:@"cw_id"]];
}

-(IBAction)yesButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [popupView.layer addAnimation:transition forKey:nil];
    popupView.hidden = TRUE;
    
    PlayViewController *objPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
    objPlay.crosswordID = cwcLabel.text;
    [self.navigationController pushViewController:objPlay animated:YES];
}

-(IBAction)skipButton:(id)sender{
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [popupView.layer addAnimation:transition forKey:nil];
    popupView.hidden = TRUE;
}

-(void)rateButtonClicked:(UIButton*)sender {

    CATransition *transition2 = [CATransition animation];
    transition2.duration = 0.3;
    transition2.type = kCATransitionFade;
    transition2.subtype = kCATransitionFromRight;
    [transition2 setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [rateView.layer addAnimation:transition2 forKey:nil];
    rateView.hidden = FALSE;
    
    NSDictionary *playdictionary = [historyArray objectAtIndex:sender.tag];
    
    ratechangeview.value  = [[playdictionary valueForKeyPath:@"rating"] floatValue];
    
    IdString = [playdictionary valueForKeyPath:@"id"];
    
    cwcidString = [playdictionary valueForKeyPath:@"cw_id"];
    
    ratetopicLabel.text = [NSString stringWithFormat:@"%@",[playdictionary valueForKeyPath:@"topic"]];
    
    ratecwcLabel.text = cwcidString;
}

-(IBAction)rateButton:(id)sender {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=playrating&player_id=%@&id=%@&cw_id=%@&rating=%f&loginsession=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[IdString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[cwcidString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],ratechangeview.value,[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"RatedDictionary : %@",resultDictionary);
        
        if ([[resultDictionary objectForKey:@"error"] isEqualToString:@""]) {
            
            [self.view makeToast:@"Rating Success" duration:3.0 position:CSToastPositionTop];
            
            CATransition *transition = [CATransition animation];
            transition.duration = 0.3;
            transition.type = kCATransitionFade;
            transition.subtype = kCATransitionFromRight;
            [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
            [rateView.layer addAnimation:transition forKey:nil];
            rateView.hidden = TRUE;
            
            [self gamewithpageApi];
        }
    }];
}

-(IBAction)skiprateButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionFade;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [rateView.layer addAnimation:transition forKey:nil];
    
    rateView.hidden = TRUE;
}

-(IBAction)changepasswordButton:(id)sender {
    
    ChangePasswordViewController *objChange = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    [self.navigationController pushViewController:objChange animated:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (displayText.text.length == 0) {
        
        [self.view makeToast:@"Please enter name" duration:3.0 position:CSToastPositionTop];
        [displayText becomeFirstResponder];
    }
    else if (genderText.text.length== 0) {
        
        [self.view makeToast:@"Please enter your gender" duration:3.0 position:CSToastPositionTop];
        [genderText becomeFirstResponder];
    }
    else if (publicprofileText.text.length==0) {
        
        [self.view makeToast:@"Please enter your profile url" duration:3.0 position:CSToastPositionTop];
        [publicprofileText becomeFirstResponder];
    }
    else if (locationText.text.length==0) {
        
        [self.view makeToast:@"Please enter your location" duration:3.0 position:CSToastPositionTop];
        [locationText becomeFirstResponder];
    }
    else if (addressText.text.length==0) {
        
        [self.view makeToast:@"Please enter your address" duration:3.0 position:CSToastPositionTop];
    }
    else if (prizelocationText.text.length==0) {
        
        [self.view makeToast:@"Please enter your prize Delivery Address" duration:3.0 position:CSToastPositionTop];
        [prizelocationText becomeFirstResponder];
    }
    else if (pinText.text.length==0) {
        
        [self.view makeToast:@"Please enter your pin" duration:3.0 position:CSToastPositionTop];
        [pinText becomeFirstResponder];
    }
    else if (phonenumber1Text.text.length==0) {
        
        [self.view makeToast:@"Please enter your phonenumber" duration:3.0 position:CSToastPositionTop];
        [phonenumber1Text becomeFirstResponder];
    }
    else {
    
        [displayText resignFirstResponder];
        [genderText resignFirstResponder];
        [publicprofileText resignFirstResponder];
        [locationText resignFirstResponder];
        [prizelocationText resignFirstResponder];
        [pinText resignFirstResponder];
        [phonenumber1Text resignFirstResponder];
        [phonenumber2Text resignFirstResponder];
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(updatedetails) withObject:spinner afterDelay:0.0];
    }
    return YES;
}

-(void)updatedetails {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=myaccount_settings&player_id=%@&loginsession=%@&displayname=%@&crosswordreminder=%@&gender=%@&age=%@&publicprofile=%@&displaylocation=%@&prizedeliveryaddr=%@&prizedeliverydistrict=%@&prizedeliverypin=%@&prizedeliveryph1=%@&prizedeliveryph2=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"],[[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"],[displayText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], stateString,[genderText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[agegroupLabel.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[publicprofileText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[locationText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],                          [addressText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[prizelocationText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[pinText.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[phonenumber1Text.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],[phonenumber2Text.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        if ([[resultDictionary objectForKey:@"error"]isEqualToString:@""]) {
 
            [self settingsApi];
            
            [self.view makeToast:@"Update Success" duration:3.0 position:CSToastPositionTop];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
