//
//  CluesViewController.m
//  Mashithantu
//
//  Created by 2Base MacBook Pro on 28/12/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "CluesViewController.h"
#import "ClueCell.h"

@interface CluesViewController ()

@end

@implementation CluesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:downLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange(2, 3)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange(13, 3)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange(24, 3)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange(33, 3)];
    [downLabel setAttributedText:clueString];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.questionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClueCell"];
    
    cell.clueLabel.text = [NSString stringWithFormat:@"%@ %@ %@", [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"num"], [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"dir"], [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"clue"]];
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:cell.clueLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"num"]].length)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange([NSString stringWithFormat:@"%@", [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"num"]].length + 1, [NSString stringWithFormat:@"%@", [[self.questionsArray objectAtIndex:indexPath.row] objectForKey:@"dir"]].length)];
    [cell.clueLabel setAttributedText:clueString];
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.clueDelegate updateClue:[self.questionsArray objectAtIndex:indexPath.row]];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
