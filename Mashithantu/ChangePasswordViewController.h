//
//  ChangePasswordViewController.h
//  Mashithantu
//
//  Created by Nidhin Baby on 22/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface ChangePasswordViewController : UIViewController {
    
    IBOutlet TPKeyboardAvoidingScrollView *passwordScroll;
    IBOutlet UITextField *oldpasswordText,*newpasswordText,*retypepasswordText;
    IBOutlet UIButton *changeButton,*cancelButton;
    IBOutlet UIImageView *oldimageview,*newimageView,*retypeimageView;
    
    UIActivityIndicatorView *activityIndicator;
    
    WebService *webservice;
    Reachability *reach;
}

-(IBAction)changeButton:(id)sender;
-(IBAction)cancelButton:(id)sender;
@end
