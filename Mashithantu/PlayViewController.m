//
//  PlayViewController.m
//  Mashithantu
//
//  Created by 2Base MacBook Pro on 29/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "PlayViewController.h"
#import "REDPuzzleGridTile.h"
#import "REDPuzzleGridView.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "ClueCell.h"
#import "RulesViewController.h"
#import "ProfileViewController.h"
#import "TutorialViewController.h"
#import "MenuViewController.h"

@interface PlayViewController () <REDPuzzleGridViewDataSource, REDPuzzleGridViewDelegate>

@property (strong, nonatomic) REDPuzzleGridView *puzzleGridView;

@end

@implementation PlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    clearButton.enabled = NO;
    [clearButton setImage:[UIImage imageNamed:@"clear-grey"] forState:UIControlStateNormal];
    
    keypadButton.hidden = TRUE;
    keypadText.hidden = TRUE;
    
    tutorialView.hidden = TRUE;

    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [activityIndicator stopAnimating];
        //Activityindicator for loading the history
        activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        activityIndicator.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        activityIndicator.color = [UIColor whiteColor];
        activityIndicator.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:activityIndicator];
        [activityIndicator startAnimating];
        
        [self performSelector:@selector(playApi) withObject:activityIndicator afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)scoreButton:(id)sender {
    
    [self saveApi];
}

-(IBAction)menuButton:(id)sender {
    
    MenuViewController *objMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuViewController"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:objMenu animated:NO];
}

-(IBAction)clearButton:(id)sender {
    
    for (int i = 0; i < rowValue; i++) {
        
        for (int j = 0; j < columnValue; j++) {
            
            if ([[NSString stringWithFormat:@"%@", [[selectedArray objectAtIndex:j] objectAtIndex:i]] isEqualToString:@"2"] || [[[selectedArray objectAtIndex:j] objectAtIndex:i] isEqualToString:@"3"] || [[[selectedArray objectAtIndex:j] objectAtIndex:i] isEqualToString:@"4"] || [[[selectedArray objectAtIndex:j] objectAtIndex:i] isEqualToString:@"5"]) {
                
                [[selectedArray objectAtIndex:j] replaceObjectAtIndex:i withObject:@"0"];
                [[answersArray objectAtIndex:j] replaceObjectAtIndex:i withObject:@""];
            }
            else if ([[NSString stringWithFormat:@"%@", [[selectedArray objectAtIndex:j] objectAtIndex:i]] isEqualToString:@"1"]) {
                
                [[selectedArray objectAtIndex:j] replaceObjectAtIndex:i withObject:@"0"];
                [[answersArray objectAtIndex:j] replaceObjectAtIndex:i withObject:@""];
            }
        }
    }
    
    [self.puzzleGridView removeFromSuperview];
    
    self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
    self.puzzleGridView.dataSource = self;
    self.puzzleGridView.delegate = self;
    [self.view addSubview:self.puzzleGridView];
    
    clearButton.enabled = NO;
    [clearButton setImage:[UIImage imageNamed:@"clear-grey"] forState:UIControlStateNormal];
    
    [self fillTextSingle];
    
    relatedIndicesArray = [[NSMutableArray alloc] initWithArray:questionsArray];
    
    [clueTable reloadData];
}

-(IBAction)clueButton:(id)sender {
    
    CluesViewController *objClues = [self.storyboard instantiateViewControllerWithIdentifier:@"CluesViewController"];
    objClues.clueDelegate = self;
    objClues.questionsArray = questionsArray;
    [self.navigationController pushViewController:objClues animated:YES];
}

-(void)playApi {
    
    gridArray = [[NSMutableArray alloc] init];
    deadcellsArray = [[NSMutableArray alloc] init];
    questionsArray = [[NSMutableArray alloc] init];
    selectedArray = [[NSMutableArray alloc] init];
    answersArray = [[NSMutableArray alloc] init];
    relatedIndicesArray = [[NSMutableArray alloc] init];
    multipleArray = [[NSMutableArray alloc] init];
    gridtapArray = [[NSMutableArray alloc] init];
    
    if ([[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"]] isEqualToString:@"Guest"]) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=cw&cw_id=%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"], self.crosswordID] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"resultDictionary : %@",resultDictionary);
            
            rowValue = [[NSString stringWithFormat:@"%@", [resultDictionary objectForKey:@"X"]] intValue];
            
            columnValue = [[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"Y"]] intValue];
            
            for (int i = 0; i < rowValue; i++) {
                
                NSMutableArray *array1 = [[NSMutableArray alloc] init];
                NSMutableArray *array2 = [[NSMutableArray alloc] init];
                NSMutableArray *array3 = [[NSMutableArray alloc] init];
                for (int j = 0; j < columnValue; j++) {
                    
                    [array1 addObject:[NSString stringWithFormat:@"%d,%d",i,j]];
                    [array2 addObject:@"0"];
                    [array3 addObject:@""];
                }
                [gridArray insertObject:array1 atIndex:i];
                [selectedArray insertObject:array2 atIndex:i];
                [answersArray insertObject:array3 atIndex:i];
            }
            
            NSArray *gridcelltaplist = [resultDictionary objectForKey:@"gridcelltaplist"];
            
            for (int i = 0; i < rowValue; i++) {
                
                NSArray *smallArray = [gridcelltaplist subarrayWithRange:NSMakeRange(rowValue * i, columnValue)];
                [gridtapArray insertObject:smallArray atIndex:i];
            }

            NSArray *deadArray = [resultDictionary objectForKey:@"deadcells"];
            
            for (NSDictionary *deadDict in deadArray) {
                
                [deadcellsArray addObject:[NSString stringWithFormat:@"%@,%@", [deadDict objectForKey:@"y"], [deadDict objectForKey:@"x"]]];
            }
            
            [questionsArray addObjectsFromArray:[resultDictionary objectForKey:@"questions"]];
            
            
            clickedSection = [[[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] integerValue];
            clickedRow = [[[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] integerValue];
            
            typedString = @"";
            
            for (int i = 0; i < questionsArray.count; i++) {
                
                if ([[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"]] isEqualToString:[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.x"]]]) {
                    
                    [multipleArray addObject:[questionsArray objectAtIndex:i]];
                }
            }
            
            if (multipleArray.count != 0) {
                
                if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"A"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                        
                        rotationString = @"Across";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"D"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                        
                        rotationString = @"Down";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"B"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                        
                        rotationString = @"Right";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"U"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                        
                        rotationString = @"Up";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                    }
                }
                
                [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"clue"]]];
                
                NSArray *relatedIndices = [[multipleArray objectAtIndex:0] objectForKey:@"relatedIndices"];
                
                for (int k = 0; k < relatedIndices.count; k++) {
                    
                    for (int l = 0; l < questionsArray.count; l++) {
                        
                        if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                            
                            [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                        }
                    }
                }
            }
            
            self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
            self.puzzleGridView.dataSource = self;
            self.puzzleGridView.delegate = self;
            [self.view addSubview:self.puzzleGridView];
            
            [activityIndicator stopAnimating];
            [clueTable reloadData];
            
            keypadButton.hidden = FALSE;
            keypadText.hidden = FALSE;
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"TutorialKey"] == 1) {
                
                tutorialView.hidden = TRUE;
            }
            else {
                
                hidetutorialButton.layer.cornerRadius = 20;
                hidetutorialButton.layer.borderWidth = 1.0;
                hidetutorialButton.layer.borderColor = [[UIColor colorWithRed:83.0/255.0 green:233.0/255.0 blue:215.0/255.0 alpha:1.0] CGColor];
                [self.view addSubview:tutorialView];
                tutorialView.hidden = FALSE;
            }
        }];
    }
    else {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=cw&cw_id=%@&player_id=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"], self.crosswordID, [[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"resultDictionary : %@",resultDictionary);
            
            [scoreButton setTitle:[NSString stringWithFormat:@" %@", [resultDictionary objectForKey:@"score"]] forState:UIControlStateNormal];
            
            rowValue = [[NSString stringWithFormat:@"%@", [resultDictionary objectForKey:@"X"]] intValue];
            
            columnValue = [[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"Y"]] intValue];
            
            for (int i = 0; i < rowValue; i++) {
                
                NSMutableArray *array1 = [[NSMutableArray alloc] init];
                NSMutableArray *array2 = [[NSMutableArray alloc] init];
                NSMutableArray *array3 = [[NSMutableArray alloc] init];
                for (int j = 0; j < columnValue; j++) {
                    
                    [array1 addObject:[NSString stringWithFormat:@"%d,%d",i,j]];
                    [array2 addObject:@"0"];
                    [array3 addObject:@""];
                }
                [gridArray insertObject:array1 atIndex:i];
                [selectedArray insertObject:array2 atIndex:i];
                [answersArray insertObject:array3 atIndex:i];
            }
            
            
            
            ///////////////////////////////////////////////////////////////////////////////////////
            
            if ([NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"saved-answers"]].length != 0) {
                
                NSMutableArray *correctanswers = [[NSMutableArray alloc] initWithArray:[[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"saved-answers"]]  componentsSeparatedByString:@","]];
                
                NSMutableArray *savedArray = [[NSMutableArray alloc] init];
                
                for (int i = 0; i < rowValue; i++) {
                    
                    NSArray *smallArray = [correctanswers subarrayWithRange:NSMakeRange(rowValue * i, columnValue)];
                    [savedArray insertObject:smallArray atIndex:i];
                }
                
                for (int i = 0; i < rowValue; i++) {
                    
                    for (int j = 0; j < columnValue; j++) {
                        
                        if (![[NSString stringWithFormat:@"%@", [[savedArray objectAtIndex:j] objectAtIndex:i]] isEqualToString:@"0"]) {
                            
                            [[answersArray objectAtIndex:i] replaceObjectAtIndex:j withObject:[[savedArray objectAtIndex:j] objectAtIndex:i]];
                        }
                    }
                }
            }
            
            
            
            
            /////////////////////////////////////////////////////////////////////////////////////
            
            NSArray *gridcelltaplist = [resultDictionary objectForKey:@"gridcelltaplist"];
            
            for (int i = 0; i < rowValue; i++) {
                
                NSArray *smallArray = [gridcelltaplist subarrayWithRange:NSMakeRange(rowValue * i, columnValue)];
                [gridtapArray insertObject:smallArray atIndex:i];
            }
            
            NSArray *deadArray = [resultDictionary objectForKey:@"deadcells"];
            
            for (NSDictionary *deadDict in deadArray) {
                
                [deadcellsArray addObject:[NSString stringWithFormat:@"%@,%@", [deadDict objectForKey:@"y"], [deadDict objectForKey:@"x"]]];
            }
            
            [questionsArray addObjectsFromArray:[resultDictionary objectForKey:@"questions"]];
            
            
            clickedSection = [[[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] integerValue];
            clickedRow = [[[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] integerValue];
            
            typedString = @"";
            
            for (int i = 0; i < questionsArray.count; i++) {
                
                if ([[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"]] isEqualToString:[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.x"]]]) {
                    
                    [multipleArray addObject:[questionsArray objectAtIndex:i]];
                }
            }
            
            if (multipleArray.count != 0) {
                
                if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"A"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                        
                        rotationString = @"Across";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"D"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                        
                        rotationString = @"Down";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"B"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                        
                        rotationString = @"Right";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                    }
                }
                else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"U"]) {
                    
                    for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                        
                        rotationString = @"Up";
                        cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                        [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                    }
                }
                
                [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"clue"]]];
                
                NSArray *relatedIndices = [[multipleArray objectAtIndex:0] objectForKey:@"relatedIndices"];
                
                for (int k = 0; k < relatedIndices.count; k++) {
                    
                    for (int l = 0; l < questionsArray.count; l++) {
                        
                        if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                            
                            [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                        }
                    }
                }
            }
            
            self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
            self.puzzleGridView.dataSource = self;
            self.puzzleGridView.delegate = self;
            [self.view addSubview:self.puzzleGridView];
            
            [activityIndicator stopAnimating];
            [clueTable reloadData];
            
            [self saveApi];
            
            keypadButton.hidden = FALSE;
            keypadText.hidden = FALSE;
            
            if ([[NSUserDefaults standardUserDefaults] integerForKey:@"TutorialKey"] == 1) {
                
                tutorialView.hidden = TRUE;
            }
            else {
                
                hidetutorialButton.layer.cornerRadius = 20;
                hidetutorialButton.layer.borderWidth = 1.0;
                hidetutorialButton.layer.borderColor = [[UIColor colorWithRed:83.0/255.0 green:233.0/255.0 blue:215.0/255.0 alpha:1.0] CGColor];
                [self.view addSubview:tutorialView];
                tutorialView.hidden = FALSE;
            }
        }];
    }
}

- (void)viewDidLayoutSubviews
{
    self.puzzleGridView.frame = CGRectMake(0, 64.0, [[UIScreen mainScreen] bounds].size.width, self.puzzleGridView.frame.size.height);
    
    fillLabel.frame = CGRectMake(fillLabel.frame.origin.x, self.puzzleGridView.frame.size.height + 74, fillLabel.frame.size.width, fillLabel.frame.size.height);
    keypadButton.frame = CGRectMake(keypadButton.frame.origin.x, self.puzzleGridView.frame.size.height + 74, keypadButton.frame.size.width, keypadButton.frame.size.height);
    keypadText.frame = CGRectMake(keypadText.frame.origin.x, self.puzzleGridView.frame.size.height + 74, keypadText.frame.size.width, keypadText.frame.size.height);
    
    clueTable.frame = CGRectMake(clueTable.frame.origin.x, self.puzzleGridView.frame.size.height + 114, clueTable.frame.size.width, [[UIScreen mainScreen] bounds].size.height - (self.puzzleGridView.frame.size.height + 114));
}

#pragma mark - WPPuzzleGridViewDataSource

- (NSInteger)numberOfRowsInPuzzleGridView:(REDPuzzleGridView *)puzzleGridView
{
    return rowValue;
}

- (NSInteger)numberOfColumnsInPuzzleGridView:(REDPuzzleGridView *)puzzleGridView
{
    return columnValue;
}

- (REDPuzzleGridTile *)puzzleGridView:(REDPuzzleGridView *)puzzleGridView tileForIndexPath:(NSIndexPath *)indexPath
{
    REDPuzzleGridTile *tile = [[REDPuzzleGridTile alloc] init];
    tile.textLabel.text = [[answersArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

    for (int i = 0; i < deadcellsArray.count; i++) {
        
        if ([[[gridArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:[deadcellsArray objectAtIndex:i]]) {
            
            tile.userInteractionEnabled = NO;
            tile.selected = NO;
            tile.backgroundColor = [UIColor blackColor];
        }
    }
    
    for (int i = 0; i < questionsArray.count; i++) {
        
        if ([[[gridArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.x"]]]) {
            
            tile.numberLabel.text = [NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:i] objectForKey:@"num"]];
        }
    }
    
    if ([[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
        
        tile.userInteractionEnabled = YES;
        tile.selected = YES;
        tile.backgroundColor = [UIColor colorWithRed:83.0/255.0 green:233.0/255.0 blue:215.0/255.0 alpha:1.0];
    }
    else if ([[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"2"] || [[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"3"] || [[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"4"] || [[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"5"]) {
        
        tile.userInteractionEnabled = YES;
        tile.selected = YES;
        tile.backgroundColor = [UIColor colorWithRed:68.0/255.0 green:148.0/255.0 blue:177.0/255.0 alpha:1.0];
    }
    else if ([[[selectedArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] isEqualToString:@"9"]) {
        
        tile.userInteractionEnabled = YES;
        tile.selected = YES;
        tile.backgroundColor = [UIColor redColor];
    }
    
    return tile;
}

- (CGSize)sizeOfTileInPuzzleGridView:(REDPuzzleGridView *)puzzleGridView
{
    return CGSizeMake(([[UIScreen mainScreen] bounds].size.width - columnValue) / columnValue, ([[UIScreen mainScreen] bounds].size.width - rowValue) / rowValue);
}

- (NSString *)puzzleGridView:(REDPuzzleGridView *)puzzleGridView titleForTileAtIndexPath:(NSIndexPath *)indexPath
{
    return @"";
}

#pragma mark - WPPuzzleGridViewDelegate

- (void)puzzleGridView:(REDPuzzleGridView *)puzzleGridView didSelectTiles:(NSArray *)selectedCells {
    
    NSIndexPath *tileIndexPath = [selectedCells.lastObject indexPath];
    
    clickedSection = tileIndexPath.section;
    clickedRow = tileIndexPath.row;
    
    typedString = @"";
    
    clearButton.enabled = YES;
    [clearButton setImage:[UIImage imageNamed:@"clear-red"] forState:UIControlStateNormal];
    
    if ([[[selectedArray objectAtIndex:tileIndexPath.section] objectAtIndex:tileIndexPath.row] isEqualToString:@"1"]) {
        
        multipleArray = [[NSMutableArray alloc] init];
        
//        for (int i = 0; i < questionsArray.count; i++) {
//            
//            if ([[NSString stringWithFormat:@"%ld,%ld", (long)tileIndexPath.section, (long)tileIndexPath.row] isEqualToString:[NSString stringWithFormat:@"%@,%@", [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.y"], [[questionsArray objectAtIndex:i] valueForKeyPath:@"num_pos.x"]]]) {
//                
//                [multipleArray addObject:[questionsArray objectAtIndex:i]];
//            }
//        }
        
        NSArray *tapArray = [[gridtapArray objectAtIndex:clickedRow] objectAtIndex:clickedSection];
        
        for (int i = 0; i < tapArray.count; i++) {
            
            for (int j = 0; j < questionsArray.count; j++) {
                
                if ([[NSString stringWithFormat:@"%@", [tapArray objectAtIndex:i]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:j] objectForKey:@"index"]]]) {
                    
                    [multipleArray addObject:[questionsArray objectAtIndex:j]];
                }
            }
        }
        
        if (multipleArray.count != 0) {
            
            if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"A"]) {
                
                for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                    
                    rotationString = @"Across";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                }
            }
            else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"D"]) {
                
                for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                    
                    rotationString = @"Down";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                }
            }
            else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"B"]) {
                
                for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Right";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
                }
            }
            else if ([[[multipleArray objectAtIndex:0] objectForKey:@"dir"] isEqualToString:@"U"]) {
                
                for (int j = [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:0] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Up";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:0] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
                }
            }
            
            [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:0] objectForKey:@"clue"]]];
            
            NSArray *relatedIndices = [[multipleArray objectAtIndex:0] objectForKey:@"relatedIndices"];
            
            relatedIndicesArray = [[NSMutableArray alloc] init];
            
            for (int k = 0; k < relatedIndices.count; k++) {
                
                for (int l = 0; l < questionsArray.count; l++) {
                    
                    if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                        
                        [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                    }
                }
            }
        }
    }
    else if ([[[selectedArray objectAtIndex:tileIndexPath.section] objectAtIndex:tileIndexPath.row] isEqualToString:@"2"]) {
        
        for (int i = 0; i < rowValue; i++) {
            
            NSMutableArray *array2 = [[NSMutableArray alloc] init];
            for (int j = 0; j < columnValue; j++) {
                
                if ([[NSString stringWithFormat:@"%d,%d",i,j] isEqualToString:[NSString stringWithFormat:@"%ld,%ld", (long)clickedSection, (long)clickedRow]]) {
                    
                    [array2 addObject:@"1"];
                }
                else {
                    [array2 addObject:@"0"];
                }
            }
            [selectedArray replaceObjectAtIndex:i withObject:array2];
        }
        
        if (multipleArray.count > 1) {
            
            if ([[[multipleArray objectAtIndex:1] objectForKey:@"dir"] isEqualToString:@"A"]) {
                
                for (int j = [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:1] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                    
                    rotationString = @"Across";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"3"];
                }
            }
            else if ([[[multipleArray objectAtIndex:1] objectForKey:@"dir"] isEqualToString:@"D"]) {
                
                for (int j = [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:1] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                    
                    rotationString = @"Down";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"3"];
                }
            }
            else if ([[[multipleArray objectAtIndex:1] objectForKey:@"dir"] isEqualToString:@"B"]) {
                
                for (int j = [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:1] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Right";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"3"];
                }
            }
            else if ([[[multipleArray objectAtIndex:1] objectForKey:@"dir"] isEqualToString:@"U"]) {
                
                for (int j = [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:1] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Up";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:1] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"3"];
                }
            }
            
            [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:1] objectForKey:@"clue"]]];
            
            NSArray *relatedIndices = [[multipleArray objectAtIndex:1] objectForKey:@"relatedIndices"];
            
            relatedIndicesArray = [[NSMutableArray alloc] init];
            
            for (int k = 0; k < relatedIndices.count; k++) {
                
                for (int l = 0; l < questionsArray.count; l++) {
                    
                    if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                        
                        [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                    }
                }
            }
        }
    }
    else if ([[[selectedArray objectAtIndex:tileIndexPath.section] objectAtIndex:tileIndexPath.row] isEqualToString:@"3"]) {
        
        for (int i = 0; i < rowValue; i++) {
            
            NSMutableArray *array2 = [[NSMutableArray alloc] init];
            for (int j = 0; j < columnValue; j++) {
                
                if ([[NSString stringWithFormat:@"%d,%d",i,j] isEqualToString:[NSString stringWithFormat:@"%ld,%ld", (long)clickedSection, (long)clickedRow]]) {
                    
                    [array2 addObject:@"1"];
                }
                else {
                    [array2 addObject:@"0"];
                }
            }
            [selectedArray replaceObjectAtIndex:i withObject:array2];
        }
        
        if (multipleArray.count > 2) {
            
            if ([[[multipleArray objectAtIndex:2] objectForKey:@"dir"] isEqualToString:@"A"]) {
                
                for (int j = [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:2] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                    
                    rotationString = @"Across";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"4"];
                }
            }
            else if ([[[multipleArray objectAtIndex:2] objectForKey:@"dir"] isEqualToString:@"D"]) {
                
                for (int j = [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:2] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                    
                    rotationString = @"Down";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"4"];
                }
            }
            else if ([[[multipleArray objectAtIndex:2] objectForKey:@"dir"] isEqualToString:@"B"]) {
                
                for (int j = [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:2] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Right";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"4"];
                }
            }
            else if ([[[multipleArray objectAtIndex:2] objectForKey:@"dir"] isEqualToString:@"U"]) {
                
                for (int j = [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:2] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Up";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:2] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"4"];
                }
            }
            
            [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:2] objectForKey:@"clue"]]];
            
            NSArray *relatedIndices = [[multipleArray objectAtIndex:2] objectForKey:@"relatedIndices"];
            
            relatedIndicesArray = [[NSMutableArray alloc] init];
            
            for (int k = 0; k < relatedIndices.count; k++) {
                
                for (int l = 0; l < questionsArray.count; l++) {
                    
                    if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                        
                        [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                    }
                }
            }
        }
    }
    else if ([[[selectedArray objectAtIndex:tileIndexPath.section] objectAtIndex:tileIndexPath.row] isEqualToString:@"4"]) {
        
        for (int i = 0; i < rowValue; i++) {
            
            NSMutableArray *array2 = [[NSMutableArray alloc] init];
            for (int j = 0; j < columnValue; j++) {
                
                if ([[NSString stringWithFormat:@"%d,%d",i,j] isEqualToString:[NSString stringWithFormat:@"%ld,%ld", (long)clickedSection, (long)clickedRow]]) {
                    
                    [array2 addObject:@"1"];
                }
                else {
                    [array2 addObject:@"0"];
                }
            }
            [selectedArray replaceObjectAtIndex:i withObject:array2];
        }
        
        if (multipleArray.count > 3) {
            
            if ([[[multipleArray objectAtIndex:3] objectForKey:@"dir"] isEqualToString:@"A"]) {
                
                for (int j = [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[multipleArray objectAtIndex:3] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
                    
                    rotationString = @"Across";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"5"];
                }
            }
            else if ([[[multipleArray objectAtIndex:3] objectForKey:@"dir"] isEqualToString:@"D"]) {
                
                for (int j = [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[multipleArray objectAtIndex:3] objectForKey:@"length"] intValue] + [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
                    
                    rotationString = @"Down";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"5"];
                }
            }
            else if ([[[multipleArray objectAtIndex:3] objectForKey:@"dir"] isEqualToString:@"B"]) {
                
                for (int j = [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue] - [[[multipleArray objectAtIndex:3] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Right";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"5"];
                }
            }
            else if ([[[multipleArray objectAtIndex:3] objectForKey:@"dir"] isEqualToString:@"U"]) {
                
                for (int j = [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.x"] intValue] - [[[multipleArray objectAtIndex:3] objectForKey:@"length"] intValue]; --j) {
                    
                    rotationString = @"Up";
                    cluelengthString = [NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"length"]];
                    [[selectedArray objectAtIndex:[[[multipleArray objectAtIndex:3] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"5"];
                }
            }
            
            [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[multipleArray objectAtIndex:3] objectForKey:@"clue"]]];
            
            NSArray *relatedIndices = [[multipleArray objectAtIndex:3] objectForKey:@"relatedIndices"];
            
            relatedIndicesArray = [[NSMutableArray alloc] init];
            
            for (int k = 0; k < relatedIndices.count; k++) {
                
                for (int l = 0; l < questionsArray.count; l++) {
                    
                    if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                        
                        [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
                    }
                }
            }
        }
    }
    else {
        
        selectedArray = [[NSMutableArray alloc] init];
        
        for (int i = 0; i < rowValue; i++) {
            
            NSMutableArray *array2 = [[NSMutableArray alloc] init];
            for (int j = 0; j < columnValue; j++) {
                
                if ([[NSString stringWithFormat:@"%d,%d",i,j] isEqualToString:[NSString stringWithFormat:@"%ld,%ld", (long)clickedSection, (long)clickedRow]]) {
                    
                    [array2 addObject:@"1"];
                }
                else {
                    [array2 addObject:@"0"];
                }
            }
            [selectedArray insertObject:array2 atIndex:i];
        }
        
        //clearButton.enabled = NO;
        //[clearButton setImage:[UIImage imageNamed:@"clear-grey"] forState:UIControlStateNormal];
        
        [self fillTextSingle];
        
        relatedIndicesArray = [[NSMutableArray alloc] initWithArray:questionsArray];
    }
    
    [self.puzzleGridView removeFromSuperview];
    
    self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
    self.puzzleGridView.dataSource = self;
    self.puzzleGridView.delegate = self;
    [self.view addSubview:self.puzzleGridView];
    
    [clueTable reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [relatedIndicesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ClueCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClueCell"];
    
    cell.clueLabel.text = [NSString stringWithFormat:@"%@ %@ %@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"num"], [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"], [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"clue"]];
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:cell.clueLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"num"]].length)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:239.0/255.0 green:63.0/255.0 blue:90.0/255.0 alpha:1.0] range:NSMakeRange([NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"num"]].length + 1, [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"]].length)];
    [cell.clueLabel setAttributedText:clueString];
    
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    selectedArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < rowValue; i++) {
        
        NSMutableArray *array2 = [[NSMutableArray alloc] init];
        for (int j = 0; j < columnValue; j++) {
            
            [array2 addObject:@"0"];
        }
        [selectedArray insertObject:array2 atIndex:i];
    }
    
    clickedSection = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] integerValue];
    clickedRow = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] integerValue];
    
    typedString = @"";
    
    if ([[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"] isEqualToString:@"A"]) {
        
        for (int j = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue]; j < [[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"] intValue] + [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue]; j++) {
            
            rotationString = @"Across";
            cluelengthString = [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"]];
            [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
        }
    }
    else if ([[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"] isEqualToString:@"D"]) {
        
        for (int j = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue]; j < [[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"] intValue] + [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue]; j++) {
            
            rotationString = @"Down";
            cluelengthString = [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"]];
            [[selectedArray objectAtIndex:[[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
        }
    }
    else if ([[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"] isEqualToString:@"B"]) {
        
        for (int j = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue]; j > [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue] - [[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"] intValue]; --j) {
            
            rotationString = @"Right";
            cluelengthString = [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"]];
            [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
        }
    }
    else if ([[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"] isEqualToString:@"U"]) {
        
        for (int j = [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue]; j > [[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.x"] intValue] - [[[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"] intValue]; --j) {
            
            rotationString = @"Up";
            cluelengthString = [NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"length"]];
            [[selectedArray objectAtIndex:[[[relatedIndicesArray objectAtIndex:indexPath.row] valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
        }
    }
    
    [self.puzzleGridView removeFromSuperview];
    
    self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
    self.puzzleGridView.dataSource = self;
    self.puzzleGridView.delegate = self;
    [self.view addSubview:self.puzzleGridView];
    
    clearButton.enabled = YES;
    [clearButton setImage:[UIImage imageNamed:@"clear-red"] forState:UIControlStateNormal];
    
    [self fillTextMultiple:[NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"clue"]]];
    
    NSArray *relatedIndices = [[relatedIndicesArray objectAtIndex:indexPath.row] objectForKey:@"relatedIndices"];
    
    relatedIndicesArray = [[NSMutableArray alloc] init];
    
    for (int k = 0; k < relatedIndices.count; k++) {
        
        for (int l = 0; l < questionsArray.count; l++) {
            
            if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                
                [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
            }
        }
    }
    
    [clueTable reloadData];
}

-(void)updateClue:(NSDictionary *)clueDict {
    
    selectedArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < rowValue; i++) {
        
        NSMutableArray *array2 = [[NSMutableArray alloc] init];
        for (int j = 0; j < columnValue; j++) {
            
            [array2 addObject:@"0"];
        }
        [selectedArray insertObject:array2 atIndex:i];
    }
    
    clickedSection = [[clueDict valueForKeyPath:@"num_pos.y"] integerValue];
    clickedRow = [[clueDict valueForKeyPath:@"num_pos.x"] integerValue];
    
    typedString = @"";
    
    if ([[clueDict objectForKey:@"dir"] isEqualToString:@"A"]) {
        
        for (int j = [[clueDict valueForKeyPath:@"num_pos.y"] intValue]; j < [[clueDict objectForKey:@"length"] intValue] + [[clueDict valueForKeyPath:@"num_pos.y"] intValue]; j++) {
            
            rotationString = @"Across";
            cluelengthString = [NSString stringWithFormat:@"%@", [clueDict objectForKey:@"length"]];
            [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[clueDict valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
        }
    }
    else if ([[clueDict objectForKey:@"dir"] isEqualToString:@"D"]) {
        
        for (int j = [[clueDict valueForKeyPath:@"num_pos.x"] intValue]; j < [[clueDict objectForKey:@"length"] intValue] + [[clueDict valueForKeyPath:@"num_pos.x"] intValue]; j++) {
            
            rotationString = @"Down";
            cluelengthString = [NSString stringWithFormat:@"%@", [clueDict objectForKey:@"length"]];
            [[selectedArray objectAtIndex:[[clueDict valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
        }
    }
    else if ([[clueDict objectForKey:@"dir"] isEqualToString:@"B"]) {
        
        for (int j = [[clueDict valueForKeyPath:@"num_pos.y"] intValue]; j > [[clueDict valueForKeyPath:@"num_pos.y"] intValue] - [[clueDict objectForKey:@"length"] intValue]; --j) {
            
            rotationString = @"Right";
            cluelengthString = [NSString stringWithFormat:@"%@", [clueDict objectForKey:@"length"]];
            [[selectedArray objectAtIndex:j] replaceObjectAtIndex:[[clueDict valueForKeyPath:@"num_pos.x"] intValue] withObject:@"2"];
        }
    }
    else if ([[clueDict objectForKey:@"dir"] isEqualToString:@"U"]) {
        
        for (int j = [[clueDict valueForKeyPath:@"num_pos.x"] intValue]; j > [[clueDict valueForKeyPath:@"num_pos.x"] intValue] - [[clueDict objectForKey:@"length"] intValue]; --j) {
            
            rotationString = @"Up";
            cluelengthString = [NSString stringWithFormat:@"%@", [clueDict objectForKey:@"length"]];
            [[selectedArray objectAtIndex:[[clueDict valueForKeyPath:@"num_pos.y"] intValue]] replaceObjectAtIndex:j withObject:@"2"];
        }
    }
    
    [self.puzzleGridView removeFromSuperview];
    
    self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
    self.puzzleGridView.dataSource = self;
    self.puzzleGridView.delegate = self;
    [self.view addSubview:self.puzzleGridView];
    
    clearButton.enabled = YES;
    [clearButton setImage:[UIImage imageNamed:@"clear-red"] forState:UIControlStateNormal];
    
    [self fillTextMultiple:[NSString stringWithFormat:@"%@", [clueDict objectForKey:@"num"]] directionString:[NSString stringWithFormat:@"%@", [clueDict objectForKey:@"dir"]] answerString:[NSString stringWithFormat:@"%@", [clueDict objectForKey:@"clue"]]];
    
    NSArray *relatedIndices = [clueDict objectForKey:@"relatedIndices"];
    
    relatedIndicesArray = [[NSMutableArray alloc] init];
    
    for (int k = 0; k < relatedIndices.count; k++) {
        
        for (int l = 0; l < questionsArray.count; l++) {
            
            if ([[NSString stringWithFormat:@"%@", [relatedIndices objectAtIndex:k]] isEqualToString:[NSString stringWithFormat:@"%@", [[questionsArray objectAtIndex:l] objectForKey:@"index"]]]) {
                
                [relatedIndicesArray addObject:[questionsArray objectAtIndex:l]];
            }
        }
    }
    
    [clueTable reloadData];
}

-(IBAction)keypadButton:(id)sender {
    
    [keypadText resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    keypadText.hidden = FALSE;
    
    [self saveApi];
    
    return YES;
}

-(void)saveApi {
    
    clearButton.enabled = NO;
    [clearButton setImage:[UIImage imageNamed:@"clear-grey"] forState:UIControlStateNormal];
    
    fullanswerArray = [[NSMutableArray alloc] init];
    
    NSString *answerString = @"";
    for (int i = 0; i < rowValue; i++) {
        
        for (int j = 0; j < columnValue; j++) {
            
            if ([[NSString stringWithFormat:@"%@",[[answersArray objectAtIndex:j] objectAtIndex:i]] isEqualToString:@""]) {
                
                answerString = [answerString stringByAppendingString:@"0,"];
            }
            else {
                
                answerString = [answerString stringByAppendingString:[NSString stringWithFormat:@"%@,",[[answersArray objectAtIndex:j] objectAtIndex:i]]];
            }
        }
    }

    NSString *apiString = @"";
    if ([[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"]] isEqualToString:@"Guest"]) {
        
        apiString = [NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=saveanswers&cw_id=%@&player-answer=%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"], self.crosswordID, [answerString substringToIndex:[answerString length]-1]];
    }
    else {
        
        apiString = [NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=saveanswers&cw_id=%@&player_id=%@&loginsession=%@&player-answer=%@", [[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"], self.crosswordID, [[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults]objectForKey:@"SessionId"], [answerString substringToIndex:[answerString length]-1]];
    }
    
    [webService getMethodWithOptions:apiString completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"resultDictionary : %@",resultDictionary);
        
        [scoreButton setTitle:[NSString stringWithFormat:@" %@", [resultDictionary objectForKey:@"score"]] forState:UIControlStateNormal];
        
        NSMutableArray *correctanswers = [[NSMutableArray alloc] initWithArray:[[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"correct answers"]]  componentsSeparatedByString:@","]];
        
        for (int i = 0; i < rowValue; i++) {
            
            NSArray *smallArray = [correctanswers subarrayWithRange:NSMakeRange(rowValue * i, columnValue)];
            [fullanswerArray insertObject:smallArray atIndex:i];
        }        
        
        for (int i = 0; i < rowValue; i++) {
            
            for (int j = 0; j < columnValue; j++) {
                
                if (![[NSString stringWithFormat:@"%@", [[answersArray objectAtIndex:j] objectAtIndex:i]] isEqualToString:@""]) {
                    
                    if (![[NSString stringWithFormat:@"%@", [[fullanswerArray objectAtIndex:i] objectAtIndex:j]] isEqualToString:[NSString stringWithFormat:@"%@", [[answersArray objectAtIndex:j] objectAtIndex:i]]]) {
                        
                        [[selectedArray objectAtIndex:j] replaceObjectAtIndex:i withObject:@"9"];
                    }
                }
            }
        }
        
        [self.puzzleGridView removeFromSuperview];
        
        self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
        self.puzzleGridView.dataSource = self;
        self.puzzleGridView.delegate = self;
        [self.view addSubview:self.puzzleGridView];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField == keypadText) {
        
        if ([[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"1"]) {
            
            [[answersArray objectAtIndex:clickedSection] replaceObjectAtIndex:clickedRow withObject:[NSString stringWithFormat:@"%@%@", textField.text, string]];
        }
        else if ([[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"2"] || [[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"3"] || [[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"4"] || [[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"5"] || [[[selectedArray objectAtIndex:clickedSection] objectAtIndex:clickedRow] isEqualToString:@"9"]) {
            
            typedString = [typedString stringByAppendingString:[NSString stringWithFormat:@"%@%@", textField.text, string]];

            if ([rotationString isEqualToString:@"Across"]) {
                
                [[answersArray objectAtIndex:clickedSection] replaceObjectAtIndex:clickedRow withObject:[NSString stringWithFormat:@"%@%@", textField.text, string]];
                
                if ([cluelengthString intValue] > typedString.length) {
                    
                    clickedSection = clickedSection + 1;
                }
            }
            else if ([rotationString isEqualToString:@"Down"]) {
                
                [[answersArray objectAtIndex:clickedSection] replaceObjectAtIndex:clickedRow withObject:[NSString stringWithFormat:@"%@%@", textField.text, string]];
                
                if ([cluelengthString intValue] > typedString.length) {
                    
                    clickedRow = clickedRow + 1;
                }
            }
            else if ([rotationString isEqualToString:@"Right"]) {
                
                [[answersArray objectAtIndex:clickedSection] replaceObjectAtIndex:clickedRow withObject:[NSString stringWithFormat:@"%@%@", textField.text, string]];
                
                if ([cluelengthString intValue] > typedString.length) {
                    
                    clickedSection = clickedSection - 1;
                }
            }
            else if ([rotationString isEqualToString:@"Up"]) {
                
                [[answersArray objectAtIndex:clickedSection] replaceObjectAtIndex:clickedRow withObject:[NSString stringWithFormat:@"%@%@", textField.text, string]];
                
                if ([cluelengthString intValue] > typedString.length) {
                    
                    clickedRow = clickedRow - 1;
                }
            }
        }
        
        [self.puzzleGridView removeFromSuperview];
        
        self.puzzleGridView = [[REDPuzzleGridView alloc] initWithFrame:CGRectZero];
        self.puzzleGridView.dataSource = self;
        self.puzzleGridView.delegate = self;
        [self.view addSubview:self.puzzleGridView];
        
        // Prevent crashing undo bug – see note below.
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 0;
    }
    
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"Text field did begin editing");
    keypadText.hidden = TRUE;
}

-(void)fillTextSingle {
    
    fillLabel.text = @"Fill this cell.";
    fillLabel.textColor = [UIColor redColor];
}

-(void)fillTextMultiple:(NSString *)numberString directionString:(NSString *)directionString answerString:(NSString *)answerString {
    
    if ([directionString isEqualToString:@"A"]) {
        
        directionString = @"Across";
    }
    else if ([directionString isEqualToString:@"D"]) {
        
        directionString = @"Down";
    }
   else if ([directionString isEqualToString:@"B"]) {
        
        directionString = @"Right";
    }
    else if ([directionString isEqualToString:@"U"]) {
        
        directionString = @"Up";
    }
    fillLabel.text = [NSString stringWithFormat:@"%@ %@ %@", numberString, directionString, answerString];
    fillLabel.textColor = [UIColor colorWithRed:83.0/255.0 green:233.0/255.0 blue:215.0/255.0 alpha:1.0];
    
    NSMutableAttributedString *clueString = [[NSMutableAttributedString alloc] initWithAttributedString:fillLabel.attributedText];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, numberString.length)];
    [clueString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(numberString.length + 1, directionString.length)];
    [fillLabel setAttributedText:clueString];
}

-(IBAction)tutorialButton:(id)sender {
    
    TutorialViewController *objTutorial = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    //transition.subtype = kCATransitionFromTop; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self.navigationController pushViewController:objTutorial animated:NO];
}

-(IBAction)hidetutorialButton:(id)sender {
    
    tutorialView.hidden = TRUE;
    
    [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"TutorialKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
