//
//  ViewController.m
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "ViewController.h"
#import "HomeViewController.h"
#import "LoginViewController.h"
#import "WebService.h"
#import "Reachability.h"
#import "UIView+Toast.h"
#import "Constants.h"

@interface ViewController () {
    
    WebService * webService;
    Reachability * reach;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden = YES;
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    webService = [WebService api];
}


-(void)viewWillAppear:(BOOL)animated {
    
    NSString  *currentDeviceId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    
    [[NSUserDefaults standardUserDefaults] setObject:currentDeviceId forKey:@"DeviceID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        NSTimeZone *localTime = [NSTimeZone systemTimeZone];
        NSString *newTime = [localTime abbreviation];
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=startapi&timezone=%@", currentDeviceId, newTime] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Response : %@", resultDictionary);
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"session_id"]] forKey:@"SessionId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"delete md5 strtolower pass"]] forKey:@"MD5pass"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[resultDictionary objectForKey:@"latestcrossword"]] forKey:@"CrosswordID"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }];
    }
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"AppStatus"]isEqualToString:@"AppInside"]) {
        
         HomeViewController * objhome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        [self.navigationController pushViewController:objhome animated:NO];
    }
    else {
        
        LoginViewController *objLogin = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController pushViewController:objLogin animated:NO];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
