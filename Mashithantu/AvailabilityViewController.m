//
//  AvailabilityViewController.m
//  Mashithantu
//
//  Created by Shyam Alanghat on 15/11/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "AvailabilityViewController.h"
#import "CustomCell.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "HomeViewController.h"

@interface AvailabilityViewController ()

@end

@implementation AvailabilityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden= YES;
    
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    availabilityScroll.contentSize = CGSizeMake(320, 568);
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:@"Or tap to enter another id.." attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:66.0/255.0 green:99.0/255.0 blue:114.0/255.0 alpha:1] }];
    useridText.attributedPlaceholder = str;
    
    confirmButton.layer.borderWidth = 1.0;
    confirmButton.layer.borderColor =[[UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0]CGColor];
    
    suggestionArray = [[NSMutableArray alloc]initWithArray:self.availabilityArray];
    
    useridString = @"";
    
    [useridText addTarget:self action:@selector(textFieldEditingChanged:) forControlEvents:UIControlEventEditingChanged];
}

-(void)getuserid:(NSString *)userid {
    
    useridString = userid;
}

-(IBAction)confirmButton:(id)sender {
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
    
        if ([useridString  isEqualToString:@""]) {
        
            [self.view makeToast:@"Please select a user id or enter one of your choice" duration:3.0 position:CSToastPositionTop];
        }
        else {
        
            [spinner stopAnimating];
            //Activityindicator for loading the history
            spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
            spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
            spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
            spinner.lineWidth = 5.0;
            spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
            [self.view addSubview:spinner];
            [spinner startAnimating];
            
            [self performSelector:@selector(confirmApi) withObject:spinner afterDelay:0.0];
        }
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

-(void)confirmApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=userid_check&userid=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"DeviceID"], [useridString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Res%@",resultDictionary);
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
            
            [self performSelector:@selector(registrationApi) withObject:spinner afterDelay:0.0];
        }
        else {
            
            [spinner stopAnimating];
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
        }
    }];
}

-(void)registrationApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&selection=register3rd&deviceid=%@&userid=%@&name=%@&email=%@&pass=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [useridString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], self.nameString, self.emailString, [[NSUserDefaults standardUserDefaults] objectForKey:@"MD5pass"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"Res%@",resultDictionary);
        [spinner stopAnimating];
        
        if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[resultDictionary valueForKeyPath:@"userid"] forKey:@"UserId"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [[NSUserDefaults standardUserDefaults] setObject:@"AppInside" forKey:@"AppStatus"];
            [[NSUserDefaults standardUserDefaults] synchronize];

            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.navigationController.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
            
            HomeViewController *objhome = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            [self.navigationController pushViewController:objhome animated:YES];
        }
        else {
            
            if ([resultDictionary objectForKey:@"errorstring"]) {
                
                [self.view makeToast:[resultDictionary objectForKey:@"errorstring"] duration:3.0 position:CSToastPositionTop];
            }
        }
    }];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
     return [suggestionArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];

     cell.suggestionnameLabel.text = [NSString stringWithFormat:@"%@", [suggestionArray objectAtIndex:indexPath.row]];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:252.0/255.0 green:50.0/255.0 blue:109.0/255.0 alpha:1];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self getuserid:[NSString stringWithFormat:@"%@", [suggestionArray objectAtIndex:indexPath.row]]];
}

-(void)textFieldEditingChanged:(UITextField *)text {
    
    [self getuserid:text.text];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [useridText resignFirstResponder];
    return YES;
}

// TextField animation function.
-(void)animateTextField:(UITextField *) textField up:(BOOL)up {
    
    if ([[UIScreen mainScreen] bounds].size.height == 480 || [[UIScreen mainScreen] bounds].size.height == 568) {
        
        int txtPosition = (textField.frame.origin.y + 100);
        const int movementDistance = (txtPosition < 0 ? 0 : txtPosition); // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: @"anim" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration:movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    
    [self animateTextField: textField up: NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
