//
//  LoginViewController.h
//  Mashithantu
//
//  Created by Shyam on 17/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MMMaterialDesignSpinner.h"

@interface LoginViewController : UIViewController <GIDSignInDelegate, GIDSignInUIDelegate> {
    
    IBOutlet UIButton *mashiButton,*googleButton,*siteButton,*guestButton,*fbloginButton;
    IBOutlet UIScrollView *loginScroll;
    WebService *webService;
    Reachability *reach;
    MMMaterialDesignSpinner *spinner;
    FBSDKLoginManager *loginManager;
    NSString *firstnameString,*lastnameString,*emailString,*useridString,*loginTypeString;
}

-(IBAction)mashiButton:(id)sender;
-(IBAction)siteButton:(id)sender;
-(IBAction)guestButton:(id)sender;
-(IBAction)fbloginButton:(id)sender;
- (IBAction)googlePlusButtonTouchUpInside:(id)sender;

@end
