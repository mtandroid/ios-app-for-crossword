//
//  ProfileViewController.h
//  Mashithantu
//
//  Created by Shyam on 18/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "MBCircularProgressBarView.h"
#import "HCSStarRatingView.h"
#import "TPKeyboardAvoidingScrollView.h"


@interface ProfileViewController : UIViewController{
    
    IBOutlet UIView *settingsView,*accountView,*popupView,*rateView,*helpView,*mygamesView;
    IBOutlet UILabel *agegroupLabel,*profileurlLabel,*locationLabel,*totalscoreaccountLabel,*bookpointaccountLabel,*totalaccountplayedLabel,*totalaccountcreatedLabel,*cwcLabel,*topicLabel,*ratetopicLabel,*emailLabel,*ratecwcLabel,*progressLabel,*appversionLabel;
    IBOutlet UITextField *displayText,*genderText,*publicprofileText,*locationText,*prizelocationText,*pinText,*phonenumber1Text,*phonenumber2Text,*addressText;
    IBOutlet TPKeyboardAvoidingScrollView *profileScroll;
    IBOutlet UITableView *accountTable,*mygamesTable;
    IBOutlet UIButton *settingsButton,*accountButton,*gamesButton,*yesButton,*skipButton,*rateButton,*skiprateButton,*setreminderButton,*helpButton,*changepasswordButton,*homeButton,*menuButton,*tapButton;
    WebService *webService;
    Reachability *reach;
    IBOutlet MBCircularProgressBarView *Progresstracker;
    IBOutlet HCSStarRatingView *ratechangeview;
    NSMutableArray *accountsArray,*historyArray;
    NSString *IdString,*cwcidString,*stateString;
    NSArray *emptyArray;
    int historypage;
}

@property(nonatomic,retain) NSString *nameString,*screenString;

-(IBAction)settingsButton:(id)sender;
-(IBAction)accountButton:(id)sender;
-(IBAction)gamesButton:(id)sender;
-(IBAction)yesButton:(id)sender;
-(IBAction)skipButton:(id)sender;
-(IBAction)rateButton:(id)sender;
-(IBAction)skiprateButton:(id)sender;
-(IBAction)setreminderButton:(id)sender;
-(IBAction)helpButton:(id)sender;
-(IBAction)changepasswordButton:(id)sender;
-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;
-(IBAction)tapButton:(id)sender;


@end
