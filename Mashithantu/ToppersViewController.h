//
//  ToppersViewController.h
//  Mashithantu
//
//  Created by Shyam on 25/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "WebService.h"

@interface ToppersViewController : UIViewController {
    
    IBOutlet UITableView *toppersTable;
    IBOutlet UILabel *eventnameLabel,*topicLabel;
    IBOutlet UIButton *homeButton,*menuButton;
    Reachability *reach;
    WebService *webService;
    NSMutableArray *topperArray;
    int topperpage;
}

@property(nonatomic,retain) NSString *eventString;
@property(nonatomic,retain) NSString *conditionString;

-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;

@end
