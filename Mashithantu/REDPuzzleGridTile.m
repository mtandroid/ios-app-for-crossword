//
//  REDPuzzleGridTile.m
//  Word Puzzle Grid
//
//  Created by Red Davis on 10/07/2013.
//  Copyright (c) 2013 Red Davis. All rights reserved.
//

#import "REDPuzzleGridTile.h"


@interface REDPuzzleGridTile ()

@property (strong, nonatomic) UILabel *textLabel,*numberLabel;

@end


@implementation REDPuzzleGridTile

#pragma mark - Initialization

- (id)init
{
    self = [super init];
    if (self)
    {
        self.userInteractionEnabled = YES;
        self.selected = NO;
        self.backgroundColor = [UIColor colorWithRed:97.0/255.0 green:161.0/255.0 blue:165.0/255.0 alpha:1.0];
        self.textLabel = [[UILabel alloc] init];
        self.textLabel.textAlignment = NSTextAlignmentCenter;
        self.textLabel.font = [UIFont fontWithName:@"Nunito-Regular" size:12];
        self.textLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.textLabel];
        
        self.numberLabel = [[UILabel alloc] init];
        self.numberLabel.textAlignment = NSTextAlignmentLeft;
        self.numberLabel.font = [UIFont fontWithName:@"Nunito-Light" size:6];
        self.numberLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.numberLabel];
    }
    
    return self;
}

#pragma mark - View Setup

- (void)layoutSubviews
{
    CGRect bounds = self.bounds;
    self.textLabel.frame = bounds;
    self.numberLabel.frame = CGRectMake(0, 0, 10, 10);
}

#pragma mark -

//- (void)setSelected:(BOOL)selected
//{
//    if (selected)
//        self.backgroundColor = [UIColor colorWithRed:97.0/255.0 green:161.0/255.0 blue:165.0/255.0 alpha:1.0];
//    else
//        self.backgroundColor = [UIColor colorWithRed:97.0/255.0 green:161.0/255.0 blue:165.0/255.0 alpha:1.0];
//}

@end
