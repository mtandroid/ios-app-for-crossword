//
//  PlayerViewController.m
//  Mashithantu
//
//  Created by Shyam on 26/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "PlayerViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "CustomCell.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "RulesViewController.h"

@interface PlayerViewController ()

@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    playerScroll.contentSize = CGSizeMake(320, 650);
    
    playerView.hidden = FALSE;
    performanceTable.hidden = TRUE;
    
    objMenu = [[Menu alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        [objMenu .tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu .helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu .exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [performanceButton setSelected:NO];
    [playerprofileButton setSelected:YES];
    
    [playerprofileButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    [performanceButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    

    [performanceButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [playerprofileButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(playerApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}


-(void)playerApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=profile&player_id=%@", [self.playeridString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"ProfileDictionary : %@",resultDictionary);
        
        NSArray *sectionArray = [resultDictionary objectForKey:@"sections"];
        
        if (sectionArray.count != 0) {
            
            nameLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Name"]];
            scoreLabel.text =  [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Total Score"]];
            totalplayedLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Total Crossword Played"]];
            scored100Label.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Scored 100"]];
            rank1bonusLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Rank1 Bonus"]];
            otherbonusLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Other Bonus"]];
            genderLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Gender"]];
            agegroupLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"AgeGroup"]];
            publicprofileLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"PublicProfile"]];
            locationLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"DisplayLocation"]];
            createdLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Total Crossword Published"]];
            progressLabel.text = [NSString stringWithFormat:@"%@", [[sectionArray objectAtIndex:0] objectForKey:@"Total Crossword Played"]];
            
            [Progresstracker setValue:[totalplayedLabel.text floatValue] animateWithDuration:5.0];
            [Progresstracker setMaxValue:[[[sectionArray objectAtIndex:0] objectForKey:@"TotalCrosswords"] floatValue]];
            [Progresstracker setProgressLineWidth:2.0];
            [Progresstracker setProgressAngle:100];
            [Progresstracker setProgressRotationAngle:50];
            
            playernameLabel.text = [NSString stringWithFormat:@"Top ranks of %@", [[sectionArray objectAtIndex:0] objectForKey:@"Name"]];
        }
        [spinner stopAnimating];
    }];
}

-(void)performanceApi {
    
    performanceArray = [[NSMutableArray alloc] init];
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=profile&player_id=%@", [self.playeridString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"PerformanceDictionary : %@",resultDictionary);
        
        NSArray * performancearray = [resultDictionary objectForKey:@"EventDetails"];
        
        if (performancearray.count == 0) {
            
        }
        else {
            
            [performanceArray addObjectsFromArray:performancearray];
        }
        [performanceTable reloadData];
        [spinner stopAnimating];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [performanceArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    NSDictionary * performanceDictionary = [performanceArray objectAtIndex:indexPath.row];
    
    cell.serialCircleImageView.layer.cornerRadius = cell.serialCircleImageView.frame.size.width/2;
    cell.serialCircleImageView.clipsToBounds = YES;
    cell.serialCircleImageView.layer.borderWidth = 1.0;
    cell.serialCircleImageView.layer.borderColor = [[UIColor colorWithRed:126.0/255.0 green:233.0/255.0 blue:236.0/255.0 alpha:1.0] CGColor];
    
    cell.rankLabel.text = [NSString stringWithFormat:@"%@", [performanceDictionary valueForKeyPath:@"rank"]];
    cell.eventLabel.text = [NSString stringWithFormat:@"%@", [performanceDictionary valueForKeyPath:@"event"]];
    cell.scoreLabel.text = [NSString stringWithFormat:@"%@", [performanceDictionary valueForKeyPath:@"score"]];
    
    if (indexPath.row == performanceArray.count - 1) {
        
        cell.rulesDashImageView.hidden = TRUE;
    }
    else {
        
        cell.rulesDashImageView.hidden = FALSE;
    }

    return cell;
}

-(IBAction)playerprofileButton:(id)sender {
    
    [performanceButton setSelected:NO];
    [playerprofileButton setSelected:YES];
    
    [performanceButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [playerprofileButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];

    playerView.hidden = FALSE;
    performanceTable.hidden = TRUE;

    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(playerApi) withObject:spinner afterDelay:0.0];
}

-(IBAction)performanceButton:(id)sender {
    
    [playerprofileButton setSelected:NO];
    [performanceButton setSelected:YES];

    [playerprofileButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    [performanceButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    
    playerView.hidden = TRUE;
    performanceTable.hidden = FALSE;
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(performanceApi) withObject:spinner afterDelay:0.0];
}


-(IBAction)homeButton:(id)sender { 
 
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton{
    
    objMenu.hidden = TRUE;
    
    RulesViewController *objrules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objrules animated:YES];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objprofile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objprofile.screenString = @"Profile";
    [self.navigationController pushViewController:objprofile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

-(void)exitButton {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if ([[resultDictionary objectForKey:@"errorcode"]intValue] == 200){
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
