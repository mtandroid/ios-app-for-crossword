//
//  SheduleViewController.h
//  Mashithantu
//
//  Created by Shyam on 30/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "JTCalendar.h"


@interface SheduleViewController : UIViewController<JTCalendarDelegate> {
    
    IBOutlet UIView *calenderView;
    IBOutlet UITableView *sheduleTable,*calendarTable;
    IBOutlet UIButton *calenderButton,*sheduleButton,*homeButton,*menuButton;
    WebService *webService;
    Reachability *reach;
    NSMutableArray *sheduleArray,*calendarArray,*mainArray;
    int shedulepage;
}

@property (weak, nonatomic) IBOutlet JTCalendarMenuView *calendarMenuView;
@property (weak, nonatomic) IBOutlet JTHorizontalCalendarView *calendarContentView;
@property (strong, nonatomic) JTCalendarManager *calendarManager;

-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;
-(IBAction)calenderButton:(id)sender;
-(IBAction)sheduleButton:(id)sender;

@end
