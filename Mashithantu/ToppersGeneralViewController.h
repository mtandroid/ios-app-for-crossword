//
//  ToppersGeneralViewController.h
//  Mashithantu
//
//  Created by Shyam on 26/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WebService.h"
#import "Reachability.h"
#import "Menu.h"
#import "MMMaterialDesignSpinner.h"

@interface ToppersGeneralViewController : UIViewController {
    
    IBOutlet UITableView *toppersTable;
    IBOutlet UIButton *homeButton,*menuButton;
    WebService * webService;
    Reachability * reach;
    NSMutableArray *eventtopperlistArray,*cellArray;
    int topperpage;
    Menu *objMenu;
    MMMaterialDesignSpinner *spinner;
}

-(IBAction)homeButton:(id)sender;
-(IBAction)menuButton:(id)sender;

@end
