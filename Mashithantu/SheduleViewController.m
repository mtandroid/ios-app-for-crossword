//
//  SheduleViewController.m
//  Mashithantu
//
//  Created by Shyam on 30/08/16.
//  Copyright © 2016 Shyam. All rights reserved.
//

#import "SheduleViewController.h"
#import "Constants.h"
#import "UIView+Toast.h"
#import "CustomCell.h"
#import "SVPullToRefresh.h"
#import "Menu.h"
#import "MMMaterialDesignSpinner.h"
#import "ProfileViewController.h"
#import "RulesViewController.h"
#import "PlayViewController.h"

@interface SheduleViewController () {
    
    NSString *day,*monthString,*startDate;
    NSDate *_todayDate;
    NSDate *_minDate;
    NSDate *_maxDate;
    NSDate *_dateSelected;
    NSMutableDictionary *_eventsByDate;
    Menu *objMenu;
    MMMaterialDesignSpinner *spinner;
}

@end

@implementation SheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    webService = [WebService api];
    
    reach = [Reachability reachabilityWithHostname:App_Network_URL];
    
    objMenu = [[Menu alloc]initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:objMenu];
    objMenu.hidden = TRUE;
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"UserId"] isEqualToString:@"Guest"]) {
        
        objMenu.guestView.hidden = FALSE;
        objMenu.userView.hidden = TRUE;
        
        [objMenu.tapButton2 addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton2 addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.exitButton addTarget:self action:@selector(exitButton) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        
        objMenu.guestView.hidden = TRUE;
        objMenu.userView.hidden = FALSE;
        
        [objMenu.tapButton addTarget:self action:@selector(tapButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.helpButton addTarget:self action:@selector(helpButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.myAccountButton addTarget:self action:@selector(myAccountButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.settingsButton addTarget:self action:@selector(settingsButton) forControlEvents:UIControlEventTouchUpInside];
        [objMenu.logoutButton addTarget:self action:@selector(logoutButton) forControlEvents:UIControlEventTouchUpInside];
    }
    
    _calendarManager = [JTCalendarManager new];
    _calendarManager.delegate = self;
    
    [_calendarManager setMenuView:_calendarMenuView];
    [_calendarManager setContentView:_calendarContentView];
    [_calendarManager setDate:[NSDate date]];
    
    [calenderButton setSelected:YES];
    [sheduleButton setSelected:NO];
    
    [calenderButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    [sheduleButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];

    calenderView.hidden = FALSE;
    calendarTable.hidden = FALSE;
    sheduleTable.hidden = TRUE;
    
    if ([reach isReachable] || [reach isReachableViaWiFi] || [reach isReachableViaWWAN]) {
        
        [spinner stopAnimating];
        //Activityindicator for loading the history
        spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
        spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
        spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
        spinner.lineWidth =5.0;
        spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
        [self.view addSubview:spinner];
        [spinner startAnimating];
        
        [self performSelector:@selector(calendarApi) withObject:spinner afterDelay:0.0];
    }
    else {
        
        [self.view makeToast:@"Please check your internet connection" duration:3.0 position:CSToastPositionTop];
        return;
    }
}

- (void)calendar:(JTCalendarManager *)calendar prepareMenuItemView:(UILabel *)menuItemView date:(NSDate *)date
{
    static NSDateFormatter *dateFormatter;
    if(!dateFormatter){
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = @"MMMM yyyy";
        
        dateFormatter.locale = _calendarManager.dateHelper.calendar.locale;
        dateFormatter.timeZone = _calendarManager.dateHelper.calendar.timeZone;
    }
    
    menuItemView.text = [dateFormatter stringFromDate:date];
}

- (void)calendar:(JTCalendarManager *)calendar prepareDayView:(JTCalendarDayView *)dayView
{
    dayView.hidden = NO;
    // Test if the dayView is from another month than the page
    // Use only in month mode for indicate the day of the previous or next month
    if ([dayView isFromAnotherMonth]) {
        
    }
    // Today
    else if ([_calendarManager.dateHelper date:[NSDate date] isTheSameDayThan:dayView.date]) {
        
        dayView.dotView.hidden = TRUE;
        dayView.circleView.backgroundColor = [UIColor whiteColor];
        dayView.dotView.backgroundColor = [UIColor greenColor];
        dayView.textLabel.textColor = [UIColor whiteColor];
        
        NSDateFormatter *dateFormatter2 = [_calendarManager.dateHelper createDateFormatter];
        [dateFormatter2 setDateFormat:@"yyyy'-'MM'"];
        
        monthString = [NSString stringWithFormat:@"%@",[dateFormatter2 stringFromDate:calendar.date]];
    }
    
    // Selected date
    else if (_dateSelected && [_calendarManager.dateHelper date:_dateSelected isTheSameDayThan:dayView.date]) {

        dayView.dotView.hidden = YES;
        dayView.circleView.hidden = YES;
        dayView.dotView.backgroundColor = [UIColor redColor];
        NSDateFormatter *dateFormatter = [_calendarManager.dateHelper createDateFormatter];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];;
        
        day = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:dayView.date]];

        calendarArray = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < mainArray.count; i++) {
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *date1 = [[NSDate alloc] init];
            date1 = [dateFormat dateFromString:[[mainArray objectAtIndex:i]valueForKey:@"startdate"]];
            
            NSDateFormatter *dateFormat2 = [[NSDateFormatter alloc] init];
            [dateFormat2 setDateFormat:@"yyyy-MM-dd"];
            startDate =[NSString stringWithFormat:@"%@",[dateFormat2 stringFromDate:date1]];
            if ([startDate isEqualToString:day]) {
               
                [calendarArray addObject:[mainArray objectAtIndex:i]];
            }
        }
        [calendarTable reloadData];
    }
    // Another day of the current month
    else {
        
        dayView.circleView.hidden = YES;
        dayView.dotView.hidden = TRUE;
        dayView.dotView.backgroundColor = [UIColor redColor];
        dayView.textLabel.textColor = [UIColor colorWithRed:71.0/255.0 green:100.0/255.0 blue:111.0/255.0 alpha:1];
    }
    
    // Your method to test if a date have an event for example
    if ([self haveEventForDay:dayView.date]) {
        
        dayView.dotView.hidden = FALSE;
        dayView.dotView.backgroundColor = [UIColor colorWithRed:117.0/255.0 green:231.0/255.0 blue:214.0/255.0 alpha:1.0];
    }
    else {
        dayView.dotView.hidden = TRUE;
    }
}


- (void)calendarDidLoadPreviousPage:(JTCalendarManager *)calendar {
    
    NSDateFormatter *dateFormatter2 = [_calendarManager.dateHelper createDateFormatter];
    [dateFormatter2 setDateFormat:@"yyyy'-'MM'"];
    
    monthString = [NSString stringWithFormat:@"%@",[dateFormatter2 stringFromDate:calendar.date]];
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(calendarApi) withObject:spinner afterDelay:0.0];
}


- (void)calendarDidLoadNextPage:(JTCalendarManager *)calendar {

    NSDateFormatter *dateFormatter2 = [_calendarManager.dateHelper createDateFormatter];

    [dateFormatter2 setDateFormat:@"yyyy'-'MM'"];
    
    monthString = [NSString stringWithFormat:@"%@",[dateFormatter2 stringFromDate:calendar.date]];
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner =   [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(calendarApi) withObject:spinner afterDelay:0.0];
}


- (void)calendar:(JTCalendarManager *)calendar didTouchDayView:(JTCalendarDayView *)dayView
{
    // Use to indicate the selected date
    
    _dateSelected = dayView.date;
    [_calendarManager reload];
}

- (BOOL)haveEventForDay:(NSDate *)date
{
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *key = [dateFormatter stringFromDate:date];
    if(_eventsByDate[key] && [_eventsByDate[key] count] > 0){
        return YES;
    }
    return NO;
}

- (void)createRandomEvents {
    
    _eventsByDate = [NSMutableDictionary new];
    
    for (int i = 0; i < calendarArray.count; i++) {
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *randomDate =[[NSDate alloc] init];
        randomDate = [dateFormat dateFromString:[[calendarArray objectAtIndex:i]valueForKey:@"startdate"]];
        NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        // Use the date as key for eventsByDate
        NSString *key = [dateFormatter stringFromDate:randomDate];
        if(!_eventsByDate[key]){
            _eventsByDate[key] = [NSMutableArray new];
        }
        
        [_eventsByDate[key] addObject:randomDate];
    }
    [_calendarManager reload];
}

-(IBAction)homeButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)menuButton:(id)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = FALSE;
}

-(void)tapButton {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.1;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [objMenu.layer addAnimation:transition forKey:nil];
    objMenu.hidden = TRUE;
}

-(void)helpButton {
    
    objMenu.hidden = TRUE;
    
    RulesViewController *objRules = [self.storyboard instantiateViewControllerWithIdentifier:@"RulesViewController"];
    [self.navigationController pushViewController:objRules animated:YES];
}

-(void)myAccountButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Profile";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)settingsButton {
    
    objMenu.hidden = TRUE;
    
    ProfileViewController *objProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
    objProfile.screenString = @"Settings";
    [self.navigationController pushViewController:objProfile animated:YES];
}

-(void)logoutButton {
    
    UIAlertView* logoutAlert = [[UIAlertView alloc] initWithTitle:@"Are you sure you want to logout?" message:nil delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No",nil];
    [logoutAlert show];
}

-(void)exitButton {
    
    [self.navigationController popToRootViewControllerAnimated:NO];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        
        [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/base/testapi.php?site=MASHITHANTU&deviceid=%@&selection=logout&userid=%@&loginsession=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], [[NSUserDefaults standardUserDefaults] objectForKey:@"UserId"], [[NSUserDefaults standardUserDefaults] objectForKey:@"SessionId"]] completion:^(NSDictionary *resultDictionary, NSError *error) {
            
            NSLog(@"Logout Response%@",resultDictionary);
            
            if ([[resultDictionary objectForKey:@"errorcode"] intValue] == 200) {
                
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"AppStatus"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"SessionId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"UserId"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                
                [self.navigationController popToRootViewControllerAnimated:NO];
            }
        }];
    }
}

-(IBAction)calenderButton:(id)sender {
   
    [calenderButton setSelected:YES];
    [sheduleButton setSelected:NO];
    
    [calenderButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    [sheduleButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    
    calendarTable.hidden =FALSE;
    calenderView.hidden = FALSE;
    sheduleTable.hidden = TRUE;
}

-(IBAction)sheduleButton:(id)sender {
    
    [calenderButton setSelected:NO];
    [sheduleButton setSelected:YES];
    
    [sheduleButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:1.0] forState:UIControlStateSelected];
    [calenderButton setTitleColor:[UIColor colorWithRed:53.0/255.0 green:235.0/255.0 blue:216.0/255.0 alpha:0.26] forState:UIControlStateNormal];
    

    calenderView.hidden = TRUE;
    calendarTable.hidden = TRUE;
    sheduleTable.hidden = FALSE;

    [self performSelector:@selector(sheduleList) withObject:nil afterDelay:0.0];
}

-(void)sheduleList {
    
    shedulepage = 1;
    
    sheduleArray = [[NSMutableArray alloc] init];
    
    [spinner stopAnimating];
    //Activityindicator for loading the history
    spinner = [[MMMaterialDesignSpinner alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    spinner.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin;
    spinner.tintColor = [UIColor colorWithRed:25.0/255.0 green:111.0/255.0 blue:237.0/255.0 alpha:1.0];
    spinner.lineWidth =5.0;
    spinner.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self.view addSubview:spinner];
    [spinner startAnimating];
    
    [self performSelector:@selector(sheduleApi) withObject:spinner afterDelay:0.0];
    
    __weak SheduleViewController *weakSelf = self;
    
    [sheduleTable addInfiniteScrollingWithActionHandler:^{
        
        [weakSelf sheduleApi];
    }];
}

-(void)sheduleApi {
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?selection=playlist&page=%d&pagesize=20", shedulepage] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"SheduleDictionary%@",resultDictionary);
        
        NSArray *resultArray = [resultDictionary objectForKey:@"crosswordlist"];
        
        if (resultArray.count == 0) {
            
            [self.view makeToast:@"End Of the List" duration:3.0 position:CSToastPositionTop];
        }
        else {
            
            shedulepage = shedulepage + 1;
            [sheduleArray addObjectsFromArray:resultArray];
        }
        [sheduleTable reloadData];
        [sheduleTable.infiniteScrollingView stopAnimating];
        [spinner stopAnimating];
     }];
}

-(void)calendarApi {
    
    mainArray = [[NSMutableArray alloc] init];
    calendarArray = [[NSMutableArray alloc] init];
    
    [webService getMethodWithOptions:[NSString stringWithFormat:@"crossword/testapi.php?site=MASHITHANTU&deviceid=%@&selection=playlist&monthwise=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceID"], monthString] completion:^(NSDictionary *resultDictionary, NSError *error) {
        
        NSLog(@"CalendarDictionary%@", resultDictionary);
        
        NSArray *resultArray = [resultDictionary objectForKey:@"crosswordlist"];
        
        if (resultArray.count == 0) {
            
        }
        else {
         
            [calendarArray addObjectsFromArray:resultArray];
            [mainArray addObjectsFromArray:resultArray];
        }
        [calendarTable reloadData];
        [spinner stopAnimating];
        
        [self createRandomEvents];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == sheduleTable) {
    
        return [sheduleArray count];
    }
    if (tableView == calendarTable) {
        
          return [calendarArray count];
    }
    return YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CustomCell"];
    
    if (tableView == sheduleTable) {
        
        NSDictionary *sheduleDictionary = [sheduleArray objectAtIndex:indexPath.row];
        
        cell.sheduletopicLabel.text = [NSString stringWithFormat:@"%@", [sheduleDictionary valueForKeyPath:@"topic"]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDateFormatter *ddFormatter = [[NSDateFormatter alloc] init];
        [ddFormatter setDateFormat:@"dd"];
        
        cell.calenderdateLabel.text = [NSString stringWithFormat:@"%@", [ddFormatter stringFromDate:[dateFormatter dateFromString:[NSString stringWithFormat:@"%@",[sheduleDictionary valueForKeyPath:@"startdate"]]]]];
        
        NSMutableAttributedString *colouredString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ by %@", [sheduleDictionary valueForKeyPath:@"cw_id"],[sheduleDictionary valueForKeyPath:@"created"]]];
        [colouredString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:187.0/255.0 green:194.0/255.0 blue:196.0/255.0 alpha:1.0]range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [sheduleDictionary valueForKeyPath:@"cw_id"]].length)];
        cell.cwcLabel.attributedText = colouredString;
        
        NSMutableAttributedString *colouredString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [sheduleDictionary valueForKeyPath:@"message"]]];
        [colouredString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:73.0/255.0 green:107.0/255.0 blue:125.0/255.0 alpha:1.0] range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [sheduleDictionary valueForKeyPath:@"message"]].length - [NSString stringWithFormat:@"%@", [sheduleDictionary valueForKeyPath:@"lastdate"]].length)];
        cell.statusLabel.attributedText = colouredString2;
        
        if ([[sheduleDictionary objectForKey:@"type"] isEqualToString:@"scheduled"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"sheduled.png"];
            cell.calenderdateLabel.textColor = [UIColor greenColor];
        }
        else if ([[sheduleDictionary objectForKey:@"type"] isEqualToString:@"inprogress"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"InProgress.png"];
            cell.calenderdateLabel.textColor = [UIColor colorWithRed:238.0/255.0 green:187.0/255.0 blue:18.0/255.0 alpha:1.0];
        }
        else if ([[sheduleDictionary objectForKey:@"type"] isEqualToString:@"expired"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"expired.png"];
            cell.calenderdateLabel.textColor = [UIColor colorWithRed:220.0/255.0 green:73.0/255.0 blue:95.0/255.0 alpha:1.0];
        }
    }
    if (tableView == calendarTable) {
        
        NSDictionary *calendarDictionary = [calendarArray objectAtIndex:indexPath.row];
        
        cell.sheduletopicLabel.text = [NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"topic"]];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDateFormatter *ddFormatter = [[NSDateFormatter alloc] init];
        [ddFormatter setDateFormat:@"dd"];
        
        cell.calenderdateLabel.text = [NSString stringWithFormat:@"%@", [ddFormatter stringFromDate:[dateFormatter dateFromString:[NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"startdate"]]]]];
        
        NSMutableAttributedString *colouredString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ by %@", [calendarDictionary valueForKeyPath:@"cw_id"],[calendarDictionary valueForKeyPath:@"created"]]];
        [colouredString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:187.0/255.0 green:194.0/255.0 blue:196.0/255.0 alpha:1.0]range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"cw_id"]].length)];
        cell.cwcLabel.attributedText = colouredString;
        
        NSMutableAttributedString *colouredString2 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"message"]]];
        [colouredString2 addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:73.0/255.0 green:107.0/255.0 blue:125.0/255.0 alpha:1.0] range:NSMakeRange(0, [NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"message"]].length - [NSString stringWithFormat:@"%@", [calendarDictionary valueForKeyPath:@"lastdate"]].length)];
        cell.statusLabel.attributedText = colouredString2;
        
        if ([[calendarDictionary objectForKey:@"type"] isEqualToString:@"scheduled"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"sheduled.png"];
            cell.calenderdateLabel.textColor = [UIColor greenColor];
        }
        else if ([[calendarDictionary objectForKey:@"type"] isEqualToString:@"inprogress"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"InProgress.png"];
            cell.calenderdateLabel.textColor = [UIColor colorWithRed:238.0/255.0 green:187.0/255.0 blue:18.0/255.0 alpha:1.0];
        }
        else if ([[calendarDictionary objectForKey:@"type"] isEqualToString:@"expired"]) {
            
            cell.gamestatusImageView.image = [UIImage imageNamed:@"expired.png"];
            cell.calenderdateLabel.textColor = [UIColor colorWithRed:220.0/255.0 green:73.0/255.0 blue:95.0/255.0 alpha:1.0];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == sheduleTable) {
        
        NSDictionary *sheduleDictionary = [sheduleArray objectAtIndex:indexPath.row];
        
        PlayViewController *objPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
        objPlay.crosswordID = [NSString stringWithFormat:@"%@", [sheduleDictionary objectForKey:@"cw_id"]];
        [self.navigationController pushViewController:objPlay animated:YES];
    }
    if (tableView == calendarTable) {
        
        NSDictionary *calendarDictionary = [calendarArray objectAtIndex:indexPath.row];
        
        PlayViewController *objPlay = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayViewController"];
        objPlay.crosswordID = [NSString stringWithFormat:@"%@", [calendarDictionary objectForKey:@"cw_id"]];
        [self.navigationController pushViewController:objPlay animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
